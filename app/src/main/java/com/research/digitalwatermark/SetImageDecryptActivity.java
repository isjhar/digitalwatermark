package com.research.digitalwatermark;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.research.digitalwatermark.jtransform.DoubleFFT_2D;
import com.research.digitalwatermark.lens.CaptureFragment;
import com.research.digitalwatermark.vuforia.InitTrackerTask;
import com.research.digitalwatermark.vuforia.InitVuforiaTask;
import com.research.digitalwatermark.vuforia.LoadTrackerTask;
import com.vuforia.Vuforia;

import org.apache.commons.math3.geometry.euclidean.threed.Line;

import java.io.IOException;

public class SetImageDecryptActivity extends AppCompatActivity
{
	private final int PICK_IMAGE_REQUEST = 1;
	public final int TOTAL_TASK = 4;
	private ImageView imageHolder;
	private ProgressDialog dialog;
	private LinearLayout decryptOptionsLayout;
	private FrameLayout takeAPictureLayout;
	private CaptureFragment captureFragment;



	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_set_image_decrypt);
		imageHolder = (ImageView) findViewById(R.id.image_holder);
		decryptOptionsLayout = (LinearLayout) findViewById(R.id.decrypt_options);
		takeAPictureLayout = (FrameLayout) findViewById(R.id.container);
		dialog = new ProgressDialog(this);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setMessage("Loading. Please wait...");
		dialog.setCancelable(false);
		dialog.setIndeterminate(false);
	}

	public void browse(View view)
	{
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
		{
			Uri uri = data.getData();
			try
			{
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
				processBitmap(bitmap, 6.8, 6.8, 0, 0);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			Toast.makeText(getApplicationContext(), R.string.image_not_set, Toast.LENGTH_SHORT);
		}
	}

	private void processBitmap(Bitmap bitmap, double threshold, double thresholdEnd, float angle, float scale)
	{
		if(bitmap.getWidth() % 2 != 0)
		{
			Utility.createPopup(this, R.string.error, "Image is not power of 2").show();
			return;
		}
		Log.i(Constants.log, "bitmap heigth : " + bitmap.getHeight() + ", width : " + bitmap.getWidth());
		if(bitmap.getHeight() >= 256 && bitmap.getWidth() >= 256 && bitmap.getHeight() * bitmap.getWidth() / 2 >= 256)
		{
			int color = Color.argb(0, 0, 0, 0);
			int color1 = Color.argb(255, 255, 255, 255);
			Log.i(Constants.log, String.format("min : %d, max : %d, red : %d", color, color1, Color.argb(255, 255, 0, 0)));
			pasteImageToHolder(bitmap);
			DigitalWatermark.getInstance().setEncryptImage(bitmap);
			dialog.show();
			DecodeMessageFromImageYUFFormatTask decodeTask = new DecodeMessageFromImageYUFFormatTask(bitmap, dialog, this, threshold, thresholdEnd, angle, scale);
			decodeTask.execute();
		}
		else
		{
			Toast.makeText(getApplicationContext(), R.string.image_too_small, Toast.LENGTH_SHORT);
		}
	}

	private void pasteImageToHolder(final Bitmap bitmap)
	{
		if(bitmap == null)
			return;
		imageHolder.setImageBitmap(bitmap);
	}

	public ProgressDialog getDialog()
	{
		return dialog;
	}

	public void openCamera(View view)
	{
		dialog.setProgress(0);
		dialog.show();
		if (Vuforia.isInitialized())
		{
			onInitVuforiaCompleted(ErrorCode.NO_ERROR);
		}
		else
		{
			InitVuforiaTask vuforiaTask = new InitVuforiaTask(1, TOTAL_TASK, this, dialog);
			vuforiaTask.execute();
		}
	}

	public void onInitVuforiaCompleted(int errorCode)
	{
		Log.i(Constants.log, "onInitVuforiaCompleted : " + errorCode);
		dialog.setProgress(Utility.calculateProgressBar(1, TOTAL_TASK));
		if(errorCode == ErrorCode.NO_ERROR)
		{
			InitTrackerTask task = new InitTrackerTask(2, TOTAL_TASK, this, dialog);
			task.execute();
		}
		else
		{
			onOpenCameraFailed(errorCode);
		}
	}

	public void onInitTrackerCompleted(int errorCode)
	{
		Log.i(Constants.log, "onInitTrackerCompleted : " + errorCode);
		dialog.setProgress(Utility.calculateProgressBar(2, TOTAL_TASK));
		if(errorCode == ErrorCode.NO_ERROR)
		{
			LoadTrackerTask task = new LoadTrackerTask(3, TOTAL_TASK, this, dialog);
			task.execute();
		}
		else
		{
			onOpenCameraFailed(errorCode);
		}
	}

	public void onLoadTrackerCompleted(int errorCode)
	{
		Log.i(Constants.log, "onLoadTrackerCompleted : " + errorCode);
		dialog.setProgress(Utility.calculateProgressBar(3, TOTAL_TASK));
		if(errorCode == ErrorCode.NO_ERROR)
		{
			System.gc();
			createCaptureFragment();
		}
		else
		{
			onOpenCameraFailed(errorCode);
		}
	}

	private void createCaptureFragment()
	{
		captureFragment = new CaptureFragment();
		getFragmentManager().beginTransaction().add(R.id.container, captureFragment).commit();
	}

	public void setLayoutVisible(boolean visible)
	{
		decryptOptionsLayout.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
		takeAPictureLayout.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
	}

	public void onTakenPictureCompleted(final Bitmap bitmap, final float angle, final float scale)
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				pasteImageToHolder(bitmap);
				setLayoutVisible(true);
				getFragmentManager().beginTransaction().remove(captureFragment).commit();
				processBitmap(bitmap, 6.5, 8.5, angle, scale);
			}
		});
	}

	public void onOpenCaptureFragmentFailed(String message)
	{
		setLayoutVisible(true);
		getFragmentManager().beginTransaction().remove(captureFragment).commit();
		Utility.createPopup(this, R.string.error, message).show();
	}

	public void onCaptureFragmentCreated(int errorCode)
	{
		// if success
		if(errorCode == ErrorCode.NO_ERROR)
		{
			setLayoutVisible(false);
		}
		else
		{
			getFragmentManager().beginTransaction().remove(captureFragment).commit();
			Utility.createPopup(this, R.string.error, "error : " + errorCode).show();
		}
	}

	public void onOpenCameraFailed(int errorCode)
	{
		dialog.dismiss();
		Utility.createPopup(this, R.string.error, "error : " + errorCode).show();
	}




	public class DecodeMessageFromImageTask extends AsyncTask<Object, Integer, String> implements ICompleteable
	{
		private double[][] fftResult;
		private int totalTask;
		private int completedTask;
		private boolean completed;
		private Bitmap image;
		private Context context;
		private String errorMessage;
		private HSVColor[][] hsvArr;

		private DecodeMessageFromImageTask(Bitmap image, Context context)
		{
			this.context = context;
			this.image = image;
		}

		@Override
		protected void onPreExecute()
		{
			dialog.setProgress(0);
			dialog.show();
		}

		@Override
		protected String doInBackground(Object... params)
		{
			errorMessage = null;
			totalTask = 7;
			completedTask = 0;
			completed = false;
			byte[] processedMessage = FFT();
			complete();
			if(processedMessage == null)
				return null;
			Log.i(Constants.log, "Extracted Message, length : " + processedMessage.length + ", Message : " + Utility.binariesToString(processedMessage));
			try
			{
				processedMessage = ConvolutionalCode.decode(processedMessage);
				Log.i(Constants.log, "Recoveried message, Length : " + processedMessage.length + ", Message : " + Utility.binariesToString(processedMessage));
				complete();
				processedMessage = Utility.binariesToBytes(processedMessage);
				Log.i(Constants.log, "Bytes, length : " + processedMessage.length + ", message : " + Utility.byteToString(processedMessage));
				complete();
				processedMessage = AESEncryption.decrypt(processedMessage);
				Log.i(Constants.log, "Decrypted message, length : " + processedMessage.length + ", message : " + Utility.binariesToString(processedMessage));
				complete();
				return new String(processedMessage);
			}
			catch (Exception e)
			{
				errorMessage = e.getMessage();
				Log.e(Constants.log, "Error : " + e.getMessage());
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onProgressUpdate(Integer... values)
		{
			dialog.setProgress(values[0]);
		}

		@Override
		protected void onPostExecute(String message)
		{
			dialog.dismiss();
			if(errorMessage != null && !errorMessage.isEmpty())
			{
				Utility.createPopup(context, R.string.error, errorMessage).show();
				return;
			}

			String realMessage = (message == null ? "Null" : message);
			Log.i(Constants.log, "Hidden Message : " + realMessage);
			Utility.createPopup(context, R.string.success, "Hidden Message : " + realMessage).show();
		}

		@Override
		public void complete()
		{
			completedTask++;
			publishProgress(completedTask * 100 / totalTask);
			completed = completedTask >= totalTask;
		}

		private byte[] FFT()
		{
			DoubleFFT_2D fft = new DoubleFFT_2D(image.getHeight(), image.getWidth());
			fftResult = new double[image.getHeight()][image.getWidth() * 2];
			hsvArr = new HSVColor[image.getHeight()][image.getWidth()];
			Utility.bitmapToDouble2D(image, hsvArr, fftResult);
			complete();
			Utility.printArr(fftResult);
			fft.complexForward(fftResult);
			complete();
			Utility.fftshift(fftResult);
			return decode();
		}

		private byte[] decode()
		{
			Log.i(Constants.log, "Decrypt");
			byte[] result = new byte[DigitalWatermark.MAX_MESSAGE_BIT_LENGTH];
			int bitCount = 0;
			for(int i = 0; i < result.length; i++)
			{
				double cos1 = Math.cos((i) * Math.PI / result.length);
				double sin1 = Math.sin((i) * Math.PI / result.length);
				int x1 = (image.getHeight() / 2 + 1) + (int) Math.round(DigitalWatermark.radius * cos1);
				int y1 = ((image.getWidth() / 2 + 1) + (int) Math.round(
						DigitalWatermark.radius * sin1)) * 2;
				double magnitude = Math.log(
						Math.hypot(fftResult[x1][y1], fftResult[x1][y1 + 1]));

				Log.i(Constants.log,
				      String.format("(%d, %d), frekuensi : %f, magnitude %f", x1, y1,
				                    fftResult[x1][y1], magnitude));

				if(magnitude > 8)
				{
					result[bitCount] = 0;
					bitCount++;
				}
				else if(magnitude < 8)
				{
					result[bitCount] = 1;
					bitCount++;
				}
			}
			return result;
		}
	} // end decode task class


} // end Set Image Decrypt Activity class
