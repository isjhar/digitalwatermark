package com.research.digitalwatermark.vuforia;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.research.digitalwatermark.Constants;
import com.research.digitalwatermark.ErrorCode;
import com.research.digitalwatermark.Utility;
import com.research.digitalwatermark.lens.CaptureFragment;
import com.vuforia.CameraDevice;
import com.vuforia.ObjectTracker;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;

/**
 * Created by Isjhar-pc on 1/19/2018.
 */

// An async task to start the camera and trackers
public class StartVuforiaTask extends AsyncTask<Void, Void, Boolean>
{
	private int taskNumber;
	private int totalTask;
	private int mCamera;
	private CaptureFragment mFragmet;
	private ProgressDialog mProgressDialog;

	public StartVuforiaTask(int taskNumber, int totalTask, int camera, CaptureFragment fragment, ProgressDialog progressDialog)
	{
		mCamera = camera;
		mFragmet = fragment;
		this.taskNumber = taskNumber;
		this.totalTask = totalTask;
		mProgressDialog = progressDialog;
	}

	@Override
	protected void onPreExecute()
	{
		mProgressDialog.setProgress(Utility.calculateProgressBar(this.taskNumber - 1, this.totalTask));
	}

	protected Boolean doInBackground(Void... params)
	{
		try {
			if (!CameraDevice.getInstance().init(mCamera))
			{
				Log.i(Constants.log, "flag 1");
				return false;
			}

			if (!CameraDevice.getInstance().selectVideoMode(
					CameraDevice.MODE.MODE_DEFAULT))
			{
				Log.i(Constants.log, "flag 2");
				return false;
			}

			if (!CameraDevice.getInstance().start())
			{
				Log.i(Constants.log, "flag 3");
				return false;
			}


			// Indicate if the trackers were started correctly
			Tracker objectTracker = TrackerManager.getInstance().getTracker(
					ObjectTracker.getClassType());
			if (objectTracker != null)
				objectTracker.start();
		}
		catch (Exception e)
		{
			Log.i(Constants.log, "flag 4, " + e.getMessage());
			return false;
		}
		return true;
	}

	protected void onPostExecute(Boolean result)
	{
		mFragmet.onStartARCompleted(result ? ErrorCode.NO_ERROR : ErrorCode.VUFORIA_START_AR_FAILED);
	}
}
