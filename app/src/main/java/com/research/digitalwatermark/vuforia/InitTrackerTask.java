package com.research.digitalwatermark.vuforia;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.research.digitalwatermark.ErrorCode;
import com.research.digitalwatermark.SetImageDecryptActivity;
import com.research.digitalwatermark.Utility;
import com.vuforia.ObjectTracker;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;

/**
 * Created by Isjhar-pc on 1/17/2018.
 */

// An async task to initialize trackers asynchronously
public class InitTrackerTask extends AsyncTask<Void, Integer, Boolean>
{
	private ProgressDialog mProgressDialog;
	private int taskNumber;
	private int totalTask;
	private SetImageDecryptActivity mActivity;

	public InitTrackerTask(int taskNumber, int totalTask, SetImageDecryptActivity activity, ProgressDialog dialog)
	{
		this.taskNumber = taskNumber;
		this.totalTask = totalTask;
		this.mProgressDialog = dialog;
		this.mActivity = activity;
	}

	@Override
	protected void onPreExecute()
	{
		mProgressDialog.setProgress(Utility.calculateProgressBar(this.taskNumber - 1, this.totalTask));
	}

	protected  Boolean doInBackground(Void... params)
	{
		// Indicate if the trackers were initialized correctly
		boolean result = true;

		TrackerManager tManager = TrackerManager.getInstance();
		Tracker tracker;

		// Trying to initialize the image tracker
		tracker = tManager.initTracker(ObjectTracker.getClassType());
		if (tracker == null)
		{
			result = false;
		}
		return result;
	}

	protected void onPostExecute(Boolean result)
	{
		mActivity.onInitTrackerCompleted(result ? ErrorCode.NO_ERROR : ErrorCode.VUFORIA_INIT_TRACKER_FAILED);
	}
}
