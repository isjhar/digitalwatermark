package com.research.digitalwatermark.vuforia;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.research.digitalwatermark.DigitalWatermark;
import com.research.digitalwatermark.ErrorCode;
import com.research.digitalwatermark.SetImageDecryptActivity;
import com.research.digitalwatermark.Utility;
import com.vuforia.Vuforia;

/**
 * Created by Isjhar-pc on 1/14/2018.
 */

public class InitVuforiaTask extends AsyncTask<Void, Integer, Boolean>
{
	private int taskNumber;
	private int totalTask;
	private SetImageDecryptActivity mActivity;
	private ProgressDialog mProgressDialog;

	public InitVuforiaTask(int taskNumber, int totalTask, SetImageDecryptActivity activity, ProgressDialog progressDialog)
	{
		this.taskNumber = taskNumber;
		this.totalTask = totalTask;
		mActivity = activity;
		mProgressDialog = progressDialog;
	}

	@Override
	protected void onPreExecute()
	{
		mProgressDialog.setProgress(Utility.calculateProgressBar(this.taskNumber - 1, this.totalTask));
	}

	@Override
	protected Boolean doInBackground(Void... params)
	{
		Vuforia.setInitParameters(mActivity, DigitalWatermark.VUFORIA_FLAG, DigitalWatermark.VUFORIA_LICENSE);
		int progressValue = 0;
		do
		{
			progressValue = Vuforia.init();
			publishProgress(progressValue);
		}
		while(progressValue >= 0 && progressValue < 100);
		return progressValue == 100;
	}

	@Override
	protected void onProgressUpdate(Integer... values)
	{
		float progress = (float) values[0];
		progress = progress / 100;
		mProgressDialog.setProgress(Utility.calculateProgressBar(taskNumber + progress, totalTask));
	}

	@Override
	protected void onPostExecute(Boolean result)
	{
		mActivity.onInitVuforiaCompleted(
					result ? ErrorCode.NO_ERROR : ErrorCode.VUFORIA_INIT_FAILED);
	}
}
