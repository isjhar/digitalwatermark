package com.research.digitalwatermark.vuforia;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.research.digitalwatermark.Constants;
import com.research.digitalwatermark.Utility;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.content.ContentValues.TAG;

/**
 * Created by Isjhar-pc on 1/27/2018.
 */

public class Triangle
{
	// Use to access and set the view transformation
	private int mMVPMatrixHandle;


	private FloatBuffer vertexBuffer;
	private Buffer indicesBuffer;

	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	static float triangleCoords[] = {   // in counterclockwise order:
			0.0f,  0.622008459f, 0.0f, // top
			-0.5f, -0.311004243f, 0.0f, // bottom left
			0.5f, -0.311004243f, 0.0f  // bottom right
	};

	short[] indices = { 0, 1, 2 };
	// Set color with red, green, blue and alpha (opacity) values
	float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };

	private final int mProgram;

	private int mPositionHandle;
	private int mColorHandle;

	private final int vertexCount = triangleCoords.length / COORDS_PER_VERTEX;
	private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex


	int vertexHandle;
	int textureCoordHandle;
	int mvpMatrixHandle;
	int texSampler2DHandle;



	public Triangle() {
		// initialize vertex byte buffer for shape coordinates
		ByteBuffer bb = ByteBuffer.allocateDirect(
				// (number of coordinate values * 4 bytes per float)
				triangleCoords.length * 4);
		// use the device hardware's native byte order
		bb.order(ByteOrder.nativeOrder());

		// create a floating point buffer from the ByteBuffer
		vertexBuffer = bb.asFloatBuffer();
		// add the coordinates to the FloatBuffer
		vertexBuffer.put(triangleCoords);
		// set the buffer to read the first coordinate
		vertexBuffer.position(0);

		// Each short takes 2 bytes
		bb = ByteBuffer.allocateDirect(2 * indices.length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		for (short s : indices)
			bb.putShort(s);
		bb.rewind();
		indicesBuffer = bb;

		// create empty OpenGL ES Program
		mProgram = Utility.createProgramFromShaderSrc(
				CubeShaders.CUBE_MESH_VERTEX_SHADER,
				CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

		vertexHandle = GLES20.glGetAttribLocation(mProgram,
		                                          "vertexPosition");
		textureCoordHandle = GLES20.glGetAttribLocation(mProgram,
		                                                "vertexTexCoord");
		mvpMatrixHandle = GLES20.glGetUniformLocation(mProgram,
		                                              "modelViewProjectionMatrix");
		texSampler2DHandle = GLES20.glGetUniformLocation(mProgram,
		                                                 "texSampler2D");
	}


	public void draw(float[] mvpMatrix) {
		// Add program to OpenGL ES environment
		GLES20.glUseProgram(mProgram);

//		// get handle to vertex shader's vPosition member
//		mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
//
//		// Prepare the triangle coordinate data
//		GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
//		                             GLES20.GL_FLOAT, false,
//		                             vertexStride, vertexBuffer);
//
//		// Enable a handle to the triangle vertices
//		GLES20.glEnableVertexAttribArray(mPositionHandle);
//
//
//		// get handle to fragment shader's vColor member
//		mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
//
//		// Set color for drawing the triangle
//		GLES20.glUniform4fv(mColorHandle, 1, color, 0);
//
//		// get handle to shape's transformation matrix
//		mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
//		checkGlError("glGetUniformLocation");
//
//		// Pass the projection and view transformation to the shader
//		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
//		checkGlError("glUniformMatrix4fv");
//
//		// Draw the triangle
//		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);
//
//		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandle);


		GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
				                             false, vertexStride, vertexBuffer);

		GLES20.glEnableVertexAttribArray(vertexHandle);

		// activate texture 0, bind it, and pass to shader
//		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
//		                     mTextures.get(textureIndex).mTextureID[0]);
//		GLES20.glUniform1i(texSampler2DHandle, 0);

		// pass the model view matrix to the shader
		GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
		                          mvpMatrix, 0);

		// finally draw the teapot
		GLES20.glDrawElements(GLES20.GL_TRIANGLES,
		                      3, GLES20.GL_UNSIGNED_SHORT,
		                     indicesBuffer);

		// disable the enabled arrays
		GLES20.glDisableVertexAttribArray(vertexHandle);
		GLES20.glDisableVertexAttribArray(textureCoordHandle);
	}


	public static void checkGlError(String glOperation) {
		int error;
		while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			Log.e(Constants.log, glOperation + ": glError " + error);
		}
	}
}
