package com.research.digitalwatermark.vuforia;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.research.digitalwatermark.Constants;
import com.research.digitalwatermark.DigitalWatermark;
import com.research.digitalwatermark.ErrorCode;
import com.research.digitalwatermark.SetImageDecryptActivity;
import com.research.digitalwatermark.Utility;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.Trackable;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

/**
 * Created by Isjhar-pc on 1/17/2018.
 */

public class LoadTrackerTask extends AsyncTask<Void, Void, Boolean>
{
	private int taskNumber;
	private int totalTask;
	private SetImageDecryptActivity mActivity;
	private ProgressDialog mProgressDialog;

	public LoadTrackerTask(int taskNumber, int totalTask, SetImageDecryptActivity activity, ProgressDialog progressDialog)
	{
		this.taskNumber = taskNumber;
		this.totalTask = totalTask;
		mActivity = activity;
		mProgressDialog = progressDialog;
	}

	@Override
	protected void onPreExecute()
	{
		mProgressDialog.setProgress(Utility.calculateProgressBar(this.taskNumber - 1, this.totalTask));
	}

	protected Boolean doInBackground(Void... params)
	{
		TrackerManager tManager = TrackerManager.getInstance();
		ObjectTracker objectTracker = (ObjectTracker) tManager
				.getTracker(ObjectTracker.getClassType());
		if (objectTracker == null)
		{
			Log.i(Constants.log, "flag 1");
			return false;
		}

		if (DigitalWatermark.getInstance().getmCurrentDataset() == null)
			DigitalWatermark.getInstance().setmCurrentDataset(objectTracker.createDataSet());

		if (DigitalWatermark.getInstance().getmCurrentDataset() == null)
		{
			Log.i(Constants.log, "flag 2");
			return false;
		}


		if (!DigitalWatermark.getInstance().getmCurrentDataset().load(
				"digital_watermark.xml",
				STORAGE_TYPE.STORAGE_APPRESOURCE))
		{
			Log.i(Constants.log, "flag 3");
			return false;
		}

		if (!objectTracker.activateDataSet(DigitalWatermark.getInstance().getmCurrentDataset()))
		{
			Log.i(Constants.log, "flag 4");
			return false;
		}

		int numTrackables = DigitalWatermark.getInstance().getmCurrentDataset().getNumTrackables();
		for (int count = 0; count < numTrackables; count++)
		{
			Trackable trackable = DigitalWatermark.getInstance().getmCurrentDataset().getTrackable(count);
			// TODO : change this later
			if(true)
			{
				trackable.startExtendedTracking();
			}

			String name = "Current Dataset : " + trackable.getName();
			trackable.setUserData(name);
		}

		return true;
	}

	protected void onPostExecute(Boolean result)
	{


		// Done loading the tracker, update application status, send the
		// exception to check errors
//		mSessionControl.onInitARDone(vuforiaException);
		mActivity.onLoadTrackerCompleted(result ? ErrorCode.NO_ERROR : ErrorCode.VUFORIA_LOAD_TRACKER_FAILED);
	}
}
