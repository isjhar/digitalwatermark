package com.research.digitalwatermark;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Matrix;
import android.opengl.GLES20;
import android.os.Build;
import android.util.Log;
import com.vuforia.CameraCalibration;
import com.vuforia.CameraDevice;
import com.vuforia.Matrix34F;
import com.vuforia.ObjectTarget;
import com.vuforia.Renderer;
import com.vuforia.Tool;
import com.vuforia.Vec2F;
import com.vuforia.Vec3F;
import com.vuforia.VideoBackgroundConfig;
import com.vuforia.VideoMode;
import java.nio.IntBuffer;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Isjhar-pc on 4/1/2017.
 */
public class Utility
{
	public static String byteToString(byte[] bytes)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++)
		{
			if (i == 0)
			{
				sb.append(bytes[i]);
			}
			else
			{
				sb.append("-" + bytes[i]);
			}
		}
		return sb.toString();
	}

	public static String binariesToString(byte[] bytes)
	{
		StringBuilder sb = new StringBuilder();
		for (byte bit : bytes)
		{
			sb.append(bit);
		}
		return sb.toString();
	}

	public static String binariesToString(List<Byte> bytes)
	{
		StringBuilder sb = new StringBuilder();
		for (byte bit : bytes)
		{
			sb.append(bit);
		}
		return sb.toString();
	}

	public static byte[] bytesToBinaries(byte[] bytes)
	{
		List<Byte> binaries = new ArrayList<>();
		for (int i = 0; i < bytes.length; i++)
		{
			int val = bytes[i];
			for (int j = 0; j < 8; j++)
			{
				binaries.add(Byte.valueOf((byte) ((val & 128) == 0 ? 0 : 1)));
				val <<= 1;
			}
		}
		byte[] result = new byte[binaries.size()];
		for (int i = 0; i < binaries.size(); i++)
		{
			result[i] = binaries.get(i).byteValue();
		}
		return result;
	}

	public static byte[] binariesToBytes(byte[] binaries)
	{
		if (binaries.length % 8 != 0)
			throw new InvalidParameterException("binaries is not multiply by 8");
		byte[] result = new byte[binaries.length / 8];
		for (int i = 0; i < result.length; i++)
		{
			// positif
			if (binaries[i * 8] == 0)
			{
				for (int j = i * 8 + 1; j < i * 8 + 8; j++)
				{
					result[i] += Math.pow(2, 7 - (j % 8)) * binaries[j];
				}
			}
			// negatif
			else
			{
				for (int j = i * 8 + 1; j < i * 8 + 8; j++)
				{
					result[i] += Math.pow(2, 7 - (j % 8)) * (binaries[j] == 1 ? 0 : 1);
				}
				result[i] += 1;
				result[i] *= -1;
			}
		}
		return result;
	}

	public static int clamp(int val, int min, int max)
	{
		return Math.max(min, Math.min(max, val));
	}

	public static double[] bitmapToDouble1D(Bitmap image)
	{
		double[] arr = new double[image.getHeight() * image.getWidth() * 2];
		for (int i = 0; i < image.getHeight(); i++)
		{
			for (int j = 0; j < image.getWidth(); j++)
			{
				int index = i * 2 * image.getWidth() + 2 * j;
				arr[index] = image.getPixel(j, i);
				arr[index + 1] = 0;
			}
		}
		return arr;
	}

	public static void bitmapToDouble2D(Bitmap image, HSVColor[][] hsvArr, double[][] hueArr)
	{
		for (int i = 0; i < image.getWidth(); i++)
		{
			for (int j = 0; j < image.getHeight(); j++)
			{
				float[] hsv = new float[3];
				int argb = image.getPixel(i, j);
				Color.RGBToHSV(Color.red(argb), Color.green(argb), Color.blue(argb), hsv);
				hueArr[j][i * 2] = hsv[0];
				hueArr[j][i * 2 + 1] = 0;
				hsvArr[j][i] = new HSVColor(hsv[0], hsv[1], hsv[2], Color.alpha(argb));
			}
		}
	}

	public static void bitmapToYUV(Bitmap image, YUVColor[][] fullChannelArr, double[][] arr,
			int indexChannel)
	{
		ColorMatrix cm = new ColorMatrix();
		cm.setRGB2YUV();
		double[] rgbTemp = new double[3];
		final float[] yuvArray = cm.getArray();
		Log.i(Constants.log, "YUV array");
		for (int i = 0; i < yuvArray.length; i++)
		{
			Log.i(Constants.log, i + " -> " + yuvArray[i]);
		}
		int rgbCounter = 0;
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 0; j < arr[i].length; j++)
			{
				if (j % 2 == 1)
				{
					arr[i][j] = 0;
				}
				else
				{
					int color = image.getPixel(j / 2, i);
					rgbTemp[0] = Color.red(color);
					rgbTemp[1] = Color.green(color);
					rgbTemp[2] = Color.blue(color);

					double[] yuv = convertRGB2YUV(rgbTemp, yuvArray);
					fullChannelArr[i][j / 2] = new YUVColor(yuv, Color.alpha(color));
					arr[i][j] = fullChannelArr[i][j / 2].getYUV(indexChannel);

					if(rgbCounter < 100)
					{
						Log.i(Constants.log, "rgb (" + i + ", " + j + ") => " + color);
						rgbCounter++;
					}
				}
			}
		}
	}

	public static Bitmap YUVToBitmap(Bitmap image, YUVColor[][] fullArr, double[][] arr, int indexChannel)
	{

		ColorMatrix cm = new ColorMatrix();
		cm.setYUV2RGB();
		final float[] rgbColorMatrix = cm.getArray();
		double[] yuv = new double[3];
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		for (int i = 0; i < image.getHeight(); i++)
		{
			for (int j = 0; j < image.getWidth(); j++)
			{
				YUVColor yuvTemp = fullArr[i][j];
				if (indexChannel == 0)
				{
					yuv[indexChannel] = arr[i][j * 2];
					yuv[1] = yuvTemp.getYUV(1);
					yuv[2] = yuvTemp.getYUV(2);
				}
				else if (indexChannel == 1)
				{
					yuv[indexChannel] = arr[i][j * 2];
					yuv[0] = yuvTemp.getYUV(0);
					yuv[2] = yuvTemp.getYUV(2);
				}
				else
				{
					yuv[indexChannel] = arr[i][j * 2];
					yuv[0] = yuvTemp.getYUV(0);
					yuv[1] = yuvTemp.getYUV(1);
				}
				double[] rgb = convertYUVToRGB(yuv, rgbColorMatrix);

				yuvTemp.setYUV(rgb);

				if (yuvTemp.getYUV(0) < min)
				{
					min = yuvTemp.getYUV(0);
				}

				if (yuvTemp.getYUV(1) < min)
				{
					min = yuvTemp.getYUV(1);
				}

				if (yuvTemp.getYUV(2) < min)
				{
					min = yuvTemp.getYUV(2);
				}

				if (yuvTemp.getYUV(0) > max)
				{
					max = yuvTemp.getYUV(0);
				}

				if (yuvTemp.getYUV(1) > max)
				{
					max = yuvTemp.getYUV(1);
				}

				if (yuvTemp.getYUV(2) > max)
				{
					max = yuvTemp.getYUV(2);
				}
			}
		}

		// normalize
		double range = max - min;
		double targetRange = 255 - 0;
		for (int i = 0; i < image.getHeight(); i++)
		{
			for (int j = 0; j < image.getWidth(); j++)
			{
				YUVColor yuvTemp = fullArr[i][j];
				int r = (int) ((yuvTemp.getYUV(0) - min) / range * (targetRange));
				int g = (int) ((yuvTemp.getYUV(1) - min) / range * (targetRange));
				int b = (int) ((yuvTemp.getYUV(2) - min) / range * (targetRange));
				int argb = Color.argb((int) yuvTemp.getA(), r, g, b);
				image.setPixel(j, i, argb);

				if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255)
				{
					Log.i(Constants.log,
					      "Invalid -> (" + i + ", " + j + ") -> (" + r + ", " + g + ", " + b + ")");
				}
			}
		}
		return image;
	}

	public static void copyPixelsRect(Bitmap source, Bitmap destination, int x, int y)
	{
		int[] pixels = new int[source.getWidth() * source.getHeight()];
		source.getPixels(pixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
		destination.setPixels(pixels, 0, source.getWidth(), x, y, source.getWidth(), source.getHeight());
	}

	public static double[] convertRGB2YUV(double[] rgb, float[] colorMatrix)
	{
		double r = rgb[0];
		double g = rgb[1];
		double b = rgb[2];
		double[] result = new double[3];
		result[0] = colorMatrix[0] * r + colorMatrix[1] * g + colorMatrix[2] * b;
		result[1] = colorMatrix[5] * r + colorMatrix[6] * g + colorMatrix[7] * b;
		result[2] = colorMatrix[10] * r + colorMatrix[11] * g + colorMatrix[12] * b;
		return result;
	}

	public static double[] convertYUVToRGB(double[] yuv, float[] colorMatrix)
	{
		double y = yuv[0];
		double u = yuv[1];
		double v = yuv[2];

		double[] result = new double[3];
		result[0] = colorMatrix[0] * y + colorMatrix[1] * u + colorMatrix[2] * v;
		result[1] = colorMatrix[5] * y + colorMatrix[6] * u + colorMatrix[7] * v;
		result[2] = colorMatrix[10] * y + colorMatrix[11] * u + colorMatrix[12] * v;
		return result;
	}

	public static double maxY()
	{
		ColorMatrix cm = new ColorMatrix();
		cm.setRGB2YUV();
		final float[] yuvArray = cm.getArray();
		return yuvArray[0] * 255 + yuvArray[1] * 255 + yuvArray[2] * 255;
	}

	public static double clamp(double value, double min, double max)
	{
		return Math.min(Math.max(value, min), max);
	}

	public static Bitmap double1DToBitmap(double[] arr, int width, int height, Bitmap original)
	{
		Log.i(Constants.log, "Config : " + original.getConfig().toString());
		/// create new image
		Bitmap resultImg = Bitmap.createBitmap(width, height, original.getConfig());
		for (int i = 0; i < resultImg.getHeight(); i++)
		{
			for (int j = 0; j < resultImg.getWidth(); j++)
			{
				int index = i * 2 * resultImg.getWidth() + 2 * j;
//				resultImg.setPixel((j * 2), i, denormalize(arr[index]));
//				resultImg.setPixel(j * 2 + 1, i, denormalize(arr[index + 1]));
				resultImg.setPixel(j, i, (int) Math.round(arr[index]));
			}
		}
//		int color = Color.argb(255, 0, 0, 0);
//		Log.i(Constants.log, "Color : " + color);
//		resultImg.setPixel(0, 0, color);
//		color = resultImg.getPixel(0, 0);
//		Log.i(Constants.log, String.format("After (0, 0) : a:%d, r:%d, g:%d, b:%d", Color.alpha(color), Color.red(color), Color.green(color), Color.blue(color)));
		return resultImg;
	}

	public static Bitmap double2DToBitmap(HSVColor[][] hsvArr, double[][] hueArr, Bitmap original)
	{
		Bitmap image = Bitmap.createBitmap(original.getWidth(), original.getHeight(),
		                                   original.getConfig());
		for (int i = 0; i < image.getWidth(); i++)
		{
			for (int j = 0; j < image.getHeight(); j++)
			{
				HSVColor hsvTemp = hsvArr[j][i];
				double hueTemp = hueArr[j][i * 2];
//				if(!Utility.isValidHueValue((float) hueTemp))
//					Log.e(Constants.log, String.format("Invalid valid at (%d, %d) - %f", i, j, hueTemp));
				float[] hsv = new float[3];
				hsv[0] = (float) clamp(hueTemp, 0, 360);
				hsv[1] = hsvTemp.getSaturation();
				hsv[2] = hsvTemp.getValue();
				int argb = Color.HSVToColor((int) hsvTemp.getAlpha(), hsv);
				image.setPixel(i, j, argb);
			}
		}
		return image;
	}


	public static int calculateDistance(byte[] s1, byte[] s2)
	{
		int shorter = Math.min(s1.length, s2.length);
		int longest = Math.max(s1.length, s2.length);

		int result = 0;
		for (int i = 0; i < shorter; i++)
		{
			if (s1[i] != s2[i]) result++;
		}
		result += longest - shorter;
		return result;
	}

	public static AlertDialog.Builder createPopup(Context context, int title, String message)
	{
		AlertDialog.Builder builder;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
		}
		else
		{
			builder = new AlertDialog.Builder(context);
		}

		builder.setTitle(title)
				.setMessage(message)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						// continue with delete
					}
				})
				.setIcon(android.R.drawable.ic_dialog_alert);

		return builder;
	}

	public static void printArr(double[] arr)
	{
		for (int i = 0; i < arr.length; i++)
		{
			Log.i(Constants.log, "index - " + i + " : " + Math.round(arr[i]));
		}
	}

	public static void printArr(float[] arr)
	{
		for (int i = 0; i < arr.length; i++)
		{
			Log.i(Constants.log, "index - " + i + " : " + arr[i]);
		}
	}

	public static void printArr(double[][] arr)
	{
		Log.i(Constants.log, "Print Arr");

		int counter = 0;
		for (int x = 0; x < arr.length; x++)
		{
			for (int y = 0; y < arr[x].length; y += 2)
			{
				Log.i(Constants.log, String.format("(%d, %d) - ", x,
				                                   y) + " " + arr[x][y] + " + (" + arr[x][y + 1] + " i)");
				counter++;
				if (counter > 100)
					return;
			}
		}
	}

	public static void printStatistic(double[] arr)
	{
		Log.i(Constants.log, "----------Statistic-----------");
		double min = Math.log(Math.hypot(arr[0], arr[1]));
		double max = min;
		double minReal = arr[0];
		double maxReal = minReal;
		double minImaginer = arr[1];
		double maxImaginer = minImaginer;
		for (int i = 2; i < arr.length; i += 2)
		{
			min = Math.min(min, Math.log(Math.hypot(arr[i], arr[i + 1])));
			max = Math.max(max, Math.log(Math.hypot(arr[i], arr[i + 1])));
			minReal = Math.min(minReal, arr[i]);
			maxReal = Math.max(maxReal, arr[i]);
			minImaginer = Math.min(minImaginer, arr[i + 1]);
			maxImaginer = Math.max(maxImaginer, arr[i + 1]);
		}
		Log.i(Constants.log, "Magnitude");
		Log.i(Constants.log, String.format("Min : %f", min));
		Log.i(Constants.log, String.format("Max : %f", max));
		Log.i(Constants.log, "Real");
		Log.i(Constants.log, String.format("Min : %f", minReal));
		Log.i(Constants.log, String.format("Max : %f", maxReal));
		Log.i(Constants.log, "Imaginer");
		Log.i(Constants.log, String.format("Min : %f", minImaginer));
		Log.i(Constants.log, String.format("Max : %f", maxImaginer));
	}

	public static void printStatistic(double[][] arr)
	{
		Log.i(Constants.log, "----------Statistic-----------");
		double min = Math.log(Math.hypot(arr[0][0], arr[0][1]));
		double max = min;
		double minReal = arr[0][0];
		double maxReal = minReal;
		double minImaginer = arr[0][1];
		double maxImaginer = minImaginer;
		int minX = 0;
		int minY = 0;
		int maxX = 0;
		int maxY = 0;
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 2; j < arr[i].length; j += 2)
			{
				double magnitude = Math.log(Math.hypot(arr[i][j], arr[i][j + 1]));
				if(magnitude < min)
				{
					min = magnitude;
					minX = i;
					minY = j;
				}

				if(magnitude > max)
				{
					max = magnitude;
					maxX = i;
					maxY = j;
				}

				minReal = Math.min(minReal, arr[i][j]);
				maxReal = Math.max(maxReal, arr[i][j]);
				minImaginer = Math.min(minImaginer, arr[i][j + 1]);
				maxImaginer = Math.max(maxImaginer, arr[i][j + 1]);
			}
		}

		Log.i(Constants.log, "Magnitude");
		Log.i(Constants.log, String.format("Min : %f, koor (%d, %d)", min, minX, minY));
		Log.i(Constants.log, String.format("Max : %f, koor (%d, %d)", max, maxX, maxY));
		Log.i(Constants.log, "Real");
		Log.i(Constants.log, String.format("Min : %f", minReal));
		Log.i(Constants.log, String.format("Max : %f", maxReal));
		Log.i(Constants.log, "Imaginer");
		Log.i(Constants.log, String.format("Min : %f", minImaginer));
		Log.i(Constants.log, String.format("Max : %f", maxImaginer));
	}

	public static void printMinMax(double[] arr)
	{
		Log.i(Constants.log, "Statistic");
		double min = arr[0];
		double max = min;
		for (int i = 1; i < arr.length; i++)
		{
			min = Math.min(min, arr[i]);
			max = Math.max(max, arr[i]);
		}
		Log.i(Constants.log, String.format("Min : %f", min));
		Log.i(Constants.log, String.format("Max : %f", max));
	}

	public static double normalize(int value)
	{
		double penyebut = (double) 2147483647 - ((double) -2147483648);
		double pembilang = (double) value - ((double) -2147483648);
//		double hasil = pembilang / penyebut;
//		Log.i(Constants.log, String.format("Pembilang : %f, Penyebut : %f, Hasil : %f", pembilang, penyebut, hasil));
		return pembilang / penyebut;
	}

	public static int denormalize(double value)
	{
		return -2147483648 + (int) Math.round(
				clamp(value, 0, 1) * ((double) 2147483647 - ((double) -2147483648)));
	}

	public static void fftshift(double[] fftArr)
	{
		int center = fftArr.length / 2;
		for (int i = 0; i < center; i++)
		{
			double temp = fftArr[i];
			fftArr[i] = fftArr[i + center];
			fftArr[i + center] = temp;
		}
	}

	public static void fftshift(double[][] fftArr)
	{
		int centerX = fftArr[0].length / 2;
		int centerY = fftArr.length / 2;

		for (int y = 0; y < centerY; y++)
		{
			for (int x = centerX; x < fftArr[0].length; x++)
			{
				double temp = fftArr[y][x];
				fftArr[y][x] = fftArr[y + centerY][x - centerX];
				fftArr[y + centerY][x - centerX] = temp;
			}
		}

		for (int y = 0; y < centerY; y++)
		{
			for (int x = 0; x < centerX; x++)
			{
				double temp = fftArr[y][x];
				fftArr[y][x] = fftArr[y + centerY][x + centerX];
				fftArr[y + centerY][x + centerX] = temp;
			}
		}
	}

	public static boolean isValidHueValue(float value)
	{
		return value >= 0 && value < 360;
	}

	public static void normalize(double[][] arr, double targetMin, double targetMax)
	{
		Log.i(Constants.log, "Min : " + targetMin + ", Max : " + targetMax);
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 0; j < arr[i].length; j += 2)
			{
				min = Math.min(min, arr[i][j]);
				max = Math.max(max, arr[i][j]);
			}
		}

		double range = max - min;
		double targetRange = targetMax - targetMin;
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 0; j < arr[i].length; j += 2)
			{
				arr[i][j] = (arr[i][j] - min) / range * (targetRange) + targetMin;
			}
		}
	}

	public static Bitmap rotateImage(Bitmap source, float angle)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
		                           true);
	}

	public static String getCurrentDateInString()
	{
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		return df.format(c.getTime());
	}

	public static int calculateProgressBar(float current, float max)
	{
		return (int) ((current / max) * 100);
	}

	public static int loadShader(int type, String shaderCode){

		// create a vertex shader type (GLES20.GL_VERTEX_SHADER)
		// or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
		int shader = GLES20.glCreateShader(type);

		// add the source code to the shader and compile it
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);

		return shader;
	}

	public static int createProgramFromShaderSrc(String vertexShaderSrc,
			String fragmentShaderSrc)
	{
		int vertShader = initShader(GLES20.GL_VERTEX_SHADER, vertexShaderSrc);
		int fragShader = initShader(GLES20.GL_FRAGMENT_SHADER,
		                            fragmentShaderSrc);

		if (vertShader == 0 || fragShader == 0)
			return 0;

		int program = GLES20.glCreateProgram();
		if (program != 0)
		{
			GLES20.glAttachShader(program, vertShader);

			GLES20.glAttachShader(program, fragShader);

			GLES20.glLinkProgram(program);
			int[] glStatusVar = { GLES20.GL_FALSE };
			GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, glStatusVar,
			                      0);
			if (glStatusVar[0] == GLES20.GL_FALSE)
			{
				GLES20.glDeleteProgram(program);
				program = 0;
			}
		}

		return program;
	}

	static int initShader(int shaderType, String source)
	{
		int shader = GLES20.glCreateShader(shaderType);
		if (shader != 0)
		{
			GLES20.glShaderSource(shader, source);
			GLES20.glCompileShader(shader);

			int[] glStatusVar = { GLES20.GL_FALSE };
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, glStatusVar,
			                     0);
			if (glStatusVar[0] == GLES20.GL_FALSE)
			{
				GLES20.glDeleteShader(shader);
				shader = 0;
			}

		}

		return shader;
	}

	public static Bitmap grabPixels(int x, int y, int w, int h)
	{
		Log.i(Constants.log, "grabPixels width : " + w + ", height : " + h);

		int b[] = new int[w * (y + h)];
		int bt[] = new int[w * h];
		IntBuffer ib = IntBuffer.wrap(b);
		ib.position(0);

		GLES20.glReadPixels(x, 0, w, y + h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, ib);

		for (int i = 0, k = 0; i < h; i++, k++) {
			for (int j = 0; j < w; j++) {
				int pix = b[i * w + j];
				int pb = (pix >> 16) & 0xff;
				int pr = (pix << 16) & 0x00ff0000;
				int pix1 = (pix & 0xff00ff00) | pr | pb;
				bt[(h - k - 1) * w + j] = pix1;
			}
		}

		Bitmap sb = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
		return sb;
	}

	public static Vec2F cameraPointToScreenPoint(Vec2F cameraPoint, int screenWidth, int screenHeight, int screenOrientation) {
		VideoMode videoMode = CameraDevice.getInstance ().getVideoMode(CameraDevice.MODE.MODE_DEFAULT);
		VideoBackgroundConfig config = Renderer.getInstance ().getVideoBackgroundConfig();

		float ratio = (float) screenHeight / screenWidth;
		float xOffset = (int) (((float) screenWidth - config.getSize().getData()[0]) / 2) + config.getPosition().getData()[0];
		float yOffset = (int) (((float) screenHeight - config.getSize().getData()[1]) / 2) - config.getPosition().getData()[1];

		//if (isActivityInPortraitMode) {
		if(true){
			// camera image is rotated 90 degrees
			int rotatedX = videoMode.getHeight() - (int)cameraPoint.getData()[1];
			int rotatedY = (int)cameraPoint.getData()[0];

			return new Vec2F(rotatedX * config.getSize().getData()[0] / (float) videoMode.getHeight() + xOffset,
			                 rotatedY * config.getSize().getData()[1] / (float) videoMode.getWidth() + yOffset);
		} else {
			return new Vec2F((cameraPoint.getData()[0] * config.getSize().getData()[0]) / videoMode.getWidth() + xOffset,
			                 cameraPoint.getData()[1] * config.getSize().getData()[1] / videoMode.getHeight() + yOffset);
		}
	}

	public static Vec2F getMarkerCenter(CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		Vec2F cameraPoint = Tool.projectPoint(cameraCalibration, pose, new Vec3F(0, 0, 0));
		return cameraPointToScreenPoint(cameraPoint, screenWidth, screenHeight, screenOrientation);
	}

	public static Vec2F getMarkerUpperLeftCorner(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		CameraCalibration calib = cameraCalibration;
		Vec2F cameraPoint = Tool.projectPoint(calib, pose,
		                                      new Vec3F(- (marker.getSize().getData()[0] / 2.0f), marker.getSize().getData()[1] / 2.0f, 0));
		Vec2F pos = cameraPointToScreenPoint(cameraPoint, screenWidth, screenHeight, screenOrientation);
		return pos;

	}

	public static Vec2F getMarkerLowerLeftCorner(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		CameraCalibration calib = cameraCalibration;
		Vec2F cameraPoint = Tool.projectPoint(calib, pose,
		                                      new Vec3F(- (marker.getSize().getData()[0] / 2.0f),- (marker.getSize().getData()[1] / 2.0f), 0));
		Vec2F pos = cameraPointToScreenPoint(cameraPoint, screenWidth, screenHeight, screenOrientation);
		return pos;
	}

	public static Vec2F getMarkerUpperRightCorner(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		CameraCalibration calib = cameraCalibration;
		Vec2F cameraPoint = Tool.projectPoint(calib, pose,
		                                      new Vec3F(marker.getSize().getData()[0] / 2.0f,marker.getSize().getData()[1] / 2.0f, 0));
		Vec2F pos = cameraPointToScreenPoint(cameraPoint, screenWidth, screenHeight, screenOrientation);
		return pos;
	}

	public static Vec2F getMarkerLowerRightCorner(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		CameraCalibration calib = cameraCalibration;
		Vec2F cameraPoint = Tool.projectPoint(calib, pose,
		                                      new Vec3F(marker.getSize().getData()[0] / 2.0f,- (marker.getSize().getData()[1] / 2.0f), 0));
		return cameraPointToScreenPoint(cameraPoint, screenWidth, screenHeight, screenOrientation);
	}

	public static Vec2F getMarkerSize(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		Vec2F markerUpperLeftCorner = getMarkerUpperLeftCorner(marker, cameraCalibration, pose, screenWidth, screenHeight, screenOrientation);
		Vec2F markerUpperRightCorner = getMarkerUpperRightCorner(marker, cameraCalibration, pose, screenWidth, screenHeight, screenOrientation);
		Vec2F markerLowerRightCorner = getMarkerLowerRightCorner(marker, cameraCalibration, pose, screenWidth, screenHeight, screenOrientation);
		return new Vec2F(distanceVec2F(markerUpperLeftCorner, markerUpperRightCorner),
		                 distanceVec2F(markerUpperRightCorner, markerLowerRightCorner));
	}

	public static float getMarkerAngle(ObjectTarget marker, CameraCalibration cameraCalibration, Matrix34F pose, int screenWidth, int screenHeight, int screenOrientation){
		Vec2F markerLowerLeftCorner = getMarkerLowerLeftCorner(marker, cameraCalibration, pose, screenWidth, screenHeight, screenOrientation);
		Vec2F markerLowerRightCorner = getMarkerLowerRightCorner(marker, cameraCalibration, pose, screenWidth, screenHeight, screenOrientation);
		return angleVec2FinDegrees(markerLowerLeftCorner, markerLowerRightCorner);
	}

	public static float distanceVec2F(Vec2F a, Vec2F b){
		float xa = a.getData()[0];
		float ya = a.getData()[1];
		float xb = b.getData()[0];
		float yb = b.getData()[1];
		return (float)Math.hypot(xa - xb, ya - yb);
	}

	public static float angleVec2FinDegrees(Vec2F a, Vec2F b) {
		float xa = a.getData()[0];
		float ya = a.getData()[1];
		float xb = b.getData()[0];
		float yb = b.getData()[1];
		float dy = yb - ya;
		float dx = xb - xa;
		float theta = (float)Math.atan2(dy, dx); // range (-PI, PI]
		theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
		//if (theta < 0) theta = 360 + theta; // range [0, 360)
		return theta;
	}

	public static Vec2F newRotationVec2F(Vec2F p, float a)
	{
		float[] data = p.getData();
		double rad = Math.toRadians(a);
		float x = (float)((data[0] * Math.cos(rad)) - (data[1] * Math.sin(rad)));
		float y = (float)((data[1] * Math.cos(rad)) + (data[0] * Math.sin(rad)));
		return new Vec2F(x, y);
	}

	public static double magnitudeOfVec2F(Vec2F p)
	{
		return Math.hypot(p.getData()[0], p.getData()[1]);
	}

	public static int calculateErrorBit(String s1, String s2)
	{
		if(s1.length() == s2.length())
		{
			StringBuilder sb = new StringBuilder();
			int error = 0;
			for(int i = 0; i < s1.length(); i++)
			{
				if(s1.charAt(i) != s2.charAt(i))
				{
					error++;
					sb.append("1");
				}
				else
				{
					sb.append("0");
				}
			}
			Log.i(Constants.log, "bit error : " + sb.toString());
			return error;
		}
		return 9999;
	}
}
