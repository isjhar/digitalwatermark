package com.research.digitalwatermark;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.research.digitalwatermark.jtransform.DoubleFFT_2D;

/**
 * Created by Isjhar-pc on 1/13/2018.
 */

public class DecodeMessageFromImageYUFFormatTask extends AsyncTask<Object, Integer, ResultData>
		implements ICompleteable
{
	private int totalTask;
	private int completedTask;
	private boolean completed;
	private Bitmap image;
	private Context context;
	private String errorMessage;
	private ProgressDialog dialog;
	private double mThreshold;
	private double mThresholdEnd;
	private float mAngle;
	private float mScale;

	public DecodeMessageFromImageYUFFormatTask(Bitmap image, ProgressDialog dialog, Context context, double threshold, double thresholdEnd, float angle, float scale)
	{
		this.context = context;
		this.image = image;
		this.dialog = dialog;
		mThreshold = threshold;
		mThresholdEnd = thresholdEnd;
		mAngle = angle;
		mScale = scale;
	}

	@Override
	protected void onPreExecute()
	{
		dialog.setProgress(0);
	}

	@Override
	protected ResultData doInBackground(Object... params)
	{
		errorMessage = null;
		totalTask = 4;
		completedTask = 0;
		completed = false;

		int macroWidth = image.getWidth()/DigitalWatermark.MACROBLOCK_NUM;
		int macroHeight = image.getHeight()/DigitalWatermark.MACROBLOCK_NUM;
		YUVColor[][] YUVArr = new YUVColor[macroHeight][macroWidth];
		double[][] vArr = new double[macroHeight][macroWidth * 2];
		byte[] message = new byte[DigitalWatermark.MAX_MESSAGE_BIT_LENGTH];
		int centerX = macroHeight / 2 + 1;
		int centerY = macroWidth / 2 + 1;
		String recoveredMessage = null;
		MacroBlock[] macroBlocks = new MacroBlock[DigitalWatermark.MACROBLOCK_NUM * DigitalWatermark.MACROBLOCK_NUM];
		int macroBlockIndex = 0;
		boolean isValid = false;
		for (int i = 0; i < image.getHeight(); i+= macroHeight)
		{
			for (int j = 0; j < image.getWidth(); j += macroWidth)
			{
				Bitmap macroImage = Bitmap.createBitmap(image, j, i, macroWidth, macroHeight);
				FFT(macroImage, YUVArr, vArr);
				int errorBit = message.length;
				double bestThreshold = mThreshold;
				Utility.printStatistic(vArr);
				for (double k = mThreshold; k <= mThresholdEnd; k+=0.5)
				{
					for(int l = 0; l < message.length; l++)
					{
						decode(l, centerX, centerY, vArr, message, k);
					}

					Log.i(Constants.log, "Extracted Message, length : " + message.length + ", Message : " + Utility.binariesToString(message));
					for (int l = 0; l < DigitalWatermark.TARGET_BITS.length; l++)
					{
						try
						{
							//byte[] processedMessage = ConvolutionalCode.decode(message);
							//Log.i(Constants.log, "Recoveried message, Length : " + processedMessage.length + ", Message : " + Utility.binariesToString(processedMessage));
							String tagetbit = DigitalWatermark.TARGET_BITS[l];
							int currentErrorBit = Utility.calculateErrorBit(tagetbit, Utility.binariesToString(message));
							Log.i(Constants.log, "error bit : " + currentErrorBit + k);
							if(currentErrorBit < errorBit)
							{
								errorBit = currentErrorBit;
								bestThreshold = k;
							}
							isValid |= errorBit < DigitalWatermark.THRESHOLD_BIT_ERROR;
							if(errorBit == 0)
							{
								Log.i(Constants.log, "error bit : " + errorBit + ", searching end");
								break;
							}
						}
						catch (Exception e)
						{
							errorMessage = e.getMessage();
							Log.e(Constants.log, "Error : " + e.getMessage());
							e.printStackTrace();
						}
						finally
						{
							complete();
						}
					}
				}
				macroBlocks[macroBlockIndex] = new MacroBlock(errorBit, bestThreshold);
				macroBlockIndex++;
			}
		}
		return new ResultData(image, mAngle, mScale, macroBlocks, isValid);
	}

	@Override
	protected void onProgressUpdate(Integer... values)
	{
		dialog.setProgress(values[0]);
	}

	@Override
	protected void onPostExecute(ResultData message)
	{
		dialog.dismiss();
		if(errorMessage != null && !errorMessage.isEmpty())
		{
			Utility.createPopup(context, R.string.error, errorMessage).show();
			return;
		}
		Utility.createPopup(context, R.string.success, message.toString()).show();
	}

	@Override
	public void complete()
	{
		completedTask++;
		publishProgress(completedTask * 100 / totalTask);
		completed = completedTask >= totalTask;
	}

	private void FFT(Bitmap image, YUVColor[][] yuvColor, double[][] fftResult)
	{
		DoubleFFT_2D fft = new DoubleFFT_2D(image.getHeight(), image.getWidth());
		Utility.bitmapToYUV(image, yuvColor, fftResult, DigitalWatermark.INDEX_CHANNEL);
		Utility.printArr(fftResult);
		fft.complexForward(fftResult);
		Utility.fftshift(fftResult);
	}

	private void decode(int k, int centerX, int centerY, double[][] fftResult, byte[] result, double threshold)
	{
		double cos1 = Math.cos((k) * Math.PI / result.length);
		double sin1 = Math.sin((k) * Math.PI / result.length);
		int x1 = (centerX) + (int) Math.round(DigitalWatermark.radius * cos1);
		int y1 = ((centerY) + (int) Math.round(
				DigitalWatermark.radius * sin1)) * 2;
		double magnitude = Math.log(
				Math.hypot(fftResult[x1][y1], fftResult[x1][y1 + 1]));

		Log.i(Constants.log,
		      String.format("%d. Decode -> (%d, %d), frekuensi : %f, magnitude %f, centerX : %d, centerY : %d, cos1 : %f, sin1 : %f", k, x1, y1,
		                    fftResult[x1][y1], magnitude, centerX, centerY, cos1, sin1));

		if(magnitude >= threshold)
		{
			result[k] = 0;
		}
		else
		{
			result[k] = 1;
		}
	}
} // end decode task class
