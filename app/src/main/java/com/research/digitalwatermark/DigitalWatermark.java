package com.research.digitalwatermark;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.vuforia.DataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Isjhar-pc on 2/18/2017.
 */
public class DigitalWatermark
{
	private Bitmap originalImage;
	private String message;
	private Bitmap encryptImage;
	private DataSet mCurrentDataset;


	public static final int MAX_MESSAGE_LENGTH = 5;
	public static final int MAX_MESSAGE_BIT_LENGTH = 256;
	public static double LOG_MAGNITUDE_THRESHOLD = 12.4;
	public static final double DELTA = 3;
	public static final double DELTA_ZERO = 0.000002;
	public static double FREQUENCY_THRESHOLD = -251928;
	public static final int radius = 94;
	public static final int INDEX_CHANNEL = 0;
	public static final int IMAGE_WIDTH = 512;
	public static final int IMAGE_HEIGHT = 512;
	public static final boolean DRAW_QUAD_EDGE = true;
	public static final String VUFORIA_LICENSE = "ATgBe6T/////AAAAmXYevR03yknRs7ckq4FHnVExgJ4nBEd6KvnaCADa8DkOw45YwqlZlNEVsmidzEMH4bZ5ZxkKZFLm0nIMKaAqoYZFZW/pl23XKIClxl3VxlY2U22IS3ap8a5rfoY/ulzisraaXzCEIPQn1eI2tMoA2Y2aVUpuWLbnoTnnHLNTr4PHLPgTPOkT/ucS54P6bIUb5tgiPtWtvYiqW0IBbT21vbdngGT3lc8YWKs/yqbEwBQ3ZSK7cNqlKxp0M/nJW+Vf1bYlbyCQXI3M2qXprznNDHOXczfXeHbrkUSa/b/XRlwvh3lE5pK3yOCeVqx/j/IP4NMo6qb0a9kohcwG+C6wOSs/uIdPM77Ke1RZ9SPrM+O8";
	public static final int VUFORIA_FLAG = 0;
	public static final int MACROBLOCK_NUM = 2;
	public static final double ZERO_BIT_MAGNITUDE = 11;
	public static final double ONE_BIT_MAGNITUDE = -3;
	// message abc12
	public static final String TARGET_BIT = "10110111000000001100100011111000001101110001110101000011110101001001000110010011111001110111110001110001011010101010011010000000";
	public static final String[] TARGET_BITS = {
			// abc12
			"0000001110100001111101110000001110011011001101110011011100001110010110001010001010110000111001101111100110111101000100100110110000110100011111100101101111010010100001000111000011100110111110010101101111100110111101110011100101011000101011001101110000111001",
			// opq89
			"1110011011000000111010000111000000111010111101110011100101011000100110001001011000100110001001100001110011010010011011000000001101001001100010100001000100010010100010101100111010111101111110101111101011110111000000001101111110101100000000110100011100110100",
			// asd56
			"0000001101110011101000010010101111010010011011000011010010011011111010000111111001010110110011100110000111111001101111010010100010011000101011111001101111101000101000101011110100010010101100001101110011011100111010000100010010101111100101100001000111111010"
	};
	public static final int THRESHOLD_BIT_ERROR = 80;



	private static DigitalWatermark instance;


	public static DigitalWatermark getInstance()
	{
		if(instance == null)
			instance = new DigitalWatermark();
		return instance;
	}

	private DigitalWatermark()
	{

	}

	private void addErrorCorrectionCode()
	{

	}

	public void testEncode(Context context)
	{
		byte[] input = {1, 0, 0, 0, 0};
		byte[] output = ConvolutionalCode.encode(input);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < input.length; i++)
			sb.append(input[i]);
		sb.append(" -> ");
		for(int i = 0; i < output.length; i++)
			sb.append(output[i]);
		Log.i(Constants.log, sb.toString());
	}

	public Bitmap getOriginalImage()
	{
		return originalImage;
	}

	public void setOriginalImage(Bitmap originalImage)
	{
		this.originalImage = originalImage;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Bitmap getEncryptImage()
	{
		return encryptImage;
	}

	public void setEncryptImage(Bitmap encryptImage)
	{
		this.encryptImage = encryptImage;
	}

	public DataSet getmCurrentDataset()
	{
		return mCurrentDataset;
	}

	public void setmCurrentDataset(DataSet mCurrentDataset)
	{
		this.mCurrentDataset = mCurrentDataset;
	}

	public void embbedMessage(byte bitMessage, double[][] arr, int i, int j, List<Coord> insertedCoord)
	{
		double magnitude = bitMessage == 1 ? ONE_BIT_MAGNITUDE : ZERO_BIT_MAGNITUDE;
		magnitude = Math.exp(magnitude);
		embbedMagnitude(arr, i, j, insertedCoord, magnitude);
	}

	public void embbedMagnitude(double[][] arr, int i, int j, List<Coord> insertedCoord, double magnitude)
	{
		embbedAMessage(arr, i, j, insertedCoord, magnitude);

		if((i != 0 && j != 0) ||
				(i != 0 && j != arr[i].length / 2) ||
				(i != arr.length / 2 && j == 0) ||
				(i != arr.length / 2 && j != arr[i].length / 2)
				)
		{
			if(i == 0)
			{
				int pairI = i;
				int centerJ = arr[i].length / 2;
				int pairJ = 2 * centerJ - j;
				embbedAMessage(arr, pairI, pairJ, insertedCoord, magnitude);
			}
			else if (j == 0)
			{
				int centerI = arr.length / 2;
				int pairI = 2 * centerI - i;
				int pairJ = j;
				embbedAMessage(arr, pairI, pairJ, insertedCoord, magnitude);
			}
			else
			{
				int centerI = arr.length / 2;
				int centerJ = arr[centerI].length / 2;
				int pairI = 2 * centerI - i;
				int pairJ = 2 * centerJ - j;
				embbedAMessage(arr, pairI, pairJ, insertedCoord, magnitude);
			}
		}
	}

	public void embbedAMessage(double[][] arr, int i, int j, List<Coord> insertedCoord, double magnitude)
	{
		double phase = Math.atan2(arr[i][j], arr[i][j + 1]);
		arr[i][j] = magnitude * Math.sin(phase);
		arr[i][j + 1] = magnitude * Math.cos(phase);
		insertedCoord.add(new Coord(i, j));
	}

	public void setParam(double[][] arr)
	{
		int candidateCount = MAX_MESSAGE_BIT_LENGTH * 2;
		List<Coord> lowestFreqCoordList = new ArrayList<>();
		for (int i = 0; i < arr.length; i++)
		{
			for (int j = 0; j < arr[i].length; j+= 2)
			{
				if(arr[i][j] != 0 || arr[i][j + 1] != 0)
				{
					if (lowestFreqCoordList.size() < candidateCount)
					{
						if (lowestFreqCoordList.isEmpty())
						{
							lowestFreqCoordList.add(new Coord(i, j));
							FREQUENCY_THRESHOLD = arr[i][j];
						}
						else
						{
							boolean isAdd = false;
							for (int k = 0; k < lowestFreqCoordList.size(); k++)
							{
								Coord coord = lowestFreqCoordList.get(k);
								if (arr[i][j] < arr[coord.getX()][coord.getY()])
								{
									lowestFreqCoordList.add(k, new Coord(i, j));
									isAdd = true;

									Coord lastElement = lowestFreqCoordList.get(
											lowestFreqCoordList.size() - 1);
									FREQUENCY_THRESHOLD = arr[lastElement.getX()][lastElement.getY()];
									break;
								}
							}

							if (!isAdd)
							{
								lowestFreqCoordList.add(new Coord(i, j));
								FREQUENCY_THRESHOLD = arr[i][j];
							}
						}
					}
					else
					{
						boolean isAdd = false;
						for (int k = 0; k < lowestFreqCoordList.size(); k++)
						{
							Coord coord = lowestFreqCoordList.get(k);
							if (arr[i][j] < arr[coord.getX()][coord.getY()])
							{
								lowestFreqCoordList.add(k, new Coord(i, j));
								isAdd = true;
								break;
							}
						}
						if (isAdd)
						{
							lowestFreqCoordList.remove(lowestFreqCoordList.size() - 1);
							Coord lastElement = lowestFreqCoordList.get(
									lowestFreqCoordList.size() - 1);
							FREQUENCY_THRESHOLD = arr[lastElement.getX()][lastElement.getY()];
						}
					}
				}
			}
		}

		LOG_MAGNITUDE_THRESHOLD = Integer.MAX_VALUE;
		for (int i = 0; i < lowestFreqCoordList.size(); i++)
		{
			Coord coord = lowestFreqCoordList.get(i);
			double magnitude = Math.log(Math.hypot(arr[coord.getX()][coord.getY()], arr[coord.getX()][coord.getY() + 1]));
			if(magnitude < LOG_MAGNITUDE_THRESHOLD)
				LOG_MAGNITUDE_THRESHOLD = magnitude;
		}

		Log.i(Constants.log, "Set Param Completed, FREQ : " + FREQUENCY_THRESHOLD + ", LOG MAG : " + LOG_MAGNITUDE_THRESHOLD);
	}
}
