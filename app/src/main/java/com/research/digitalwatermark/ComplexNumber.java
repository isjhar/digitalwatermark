package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 5/7/2017.
 */

public class ComplexNumber
{
	private int index;
	private double real;
	private double imaginary;

	public ComplexNumber(double real, double imaginary)
	{
		this.real = real;
		this.imaginary = imaginary;
	}

	public double getReal()
	{
		return real;
	}

	public void setReal(double real)
	{
		this.real = real;
	}

	public double getImaginary()
	{
		return imaginary;
	}

	public void setImaginary(double imaginary)
	{
		this.imaginary = imaginary;
	}

	@Override
	public String toString()
	{
		return real + ", " + imaginary + "j";
	}
}
