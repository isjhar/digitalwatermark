package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 5/20/2017.
 */

public interface ICompleteable
{
	void complete();
}
