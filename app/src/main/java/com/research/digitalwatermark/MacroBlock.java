package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 3/8/2018.
 */

public class MacroBlock
{
	private int mBitError;
	private double mThreshold;

	public MacroBlock(int bitError, double threshold)
	{
		mBitError = bitError;
		mThreshold = threshold;
	}

	public int getmBitError()
	{
		return mBitError;
	}

	public void setmBitError(int mBitError)
	{
		this.mBitError = mBitError;
	}

	public double getmThreshold()
	{
		return mThreshold;
	}

	public void setmThreshold(double mThreshold)
	{
		this.mThreshold = mThreshold;
	}
}
