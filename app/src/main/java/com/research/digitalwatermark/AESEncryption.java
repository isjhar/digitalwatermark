package com.research.digitalwatermark;

import android.util.Log;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Isjhar-pc on 2/22/2017.
 */
public class AESEncryption
{
	private static final byte[] KEY = new byte[]{'D', 'i', 'g', 'i', 't',
			'a', 'l', 'w', 'a', 't', 'e', 'r', 'm', 'a', 'r', 'k'};
	private static final String ALGO = "AES";

	public static byte[] encrypt(byte[] message)
			throws Exception
	{
		Key key = new SecretKeySpec(KEY, ALGO);
		Cipher cipher = Cipher.getInstance(ALGO);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encryptedMessage = cipher.doFinal(message);
		return encryptedMessage;
	}

	public static byte[] decrypt(byte[] encryptedMessage)
			throws Exception
	{
		Key key = new SecretKeySpec(KEY, ALGO);
		Cipher cipher = Cipher.getInstance(ALGO);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] message = cipher.doFinal(encryptedMessage);
		return message;
	}
}
