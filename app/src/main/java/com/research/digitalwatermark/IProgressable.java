package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 5/11/2017.
 */

public interface IProgressable
{
	void update(int progress);
}
