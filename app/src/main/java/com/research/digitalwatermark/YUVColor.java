package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 10/27/2017.
 */

public class YUVColor
{
	private double[] yuv;
	private double a;

	public YUVColor(double yuv[], double a)
	{
		this.yuv = yuv;
		this.a = a;
	}

	public double getYUV(int index)
	{
		return yuv[index];
	}

	public void setYUV(double[] yuv)
	{
		this.yuv = yuv;
	}

	public double getA()
	{
		return a;
	}

	public void setA(double a)
	{
		this.a = a;
	}
}
