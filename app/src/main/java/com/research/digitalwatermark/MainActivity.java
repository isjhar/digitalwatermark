package com.research.digitalwatermark;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements Consumer<String>
{
	private int PICK_IMAGE_REQUEST = 1;
	private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

	    dialog = new ProgressDialog(this);
	    dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	    dialog.setMessage("Loading. Please wait...");
	    dialog.setCancelable(false);
	    dialog.setIndeterminate(false);
    }

	public void encrypt(View view)
	{
		Intent encryptIntent = new Intent(this, SetImageActivity.class);
		startActivity(encryptIntent);
	}

	public void decrypt(View view)
	{
		Intent decryptIntent = new Intent(this, SetImageDecryptActivity.class);
		startActivity(decryptIntent);
	}

	public void extract(View view)
	{
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
		{
			Uri uri = data.getData();
			try
			{
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
				ExtractTask extrackTask = new ExtractTask(this);
				extrackTask.execute(bitmap);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				Utility.createPopup(this, R.string.error, e.getMessage()).show();
			}
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

	@Override
	public void accept(String imageTxt)
	{
		// TODO : write text to file
		File documentFolder = Environment.getExternalStorageDirectory();
		File digitalWatermarkFolder = new File(documentFolder, "DigitalWatermarking");
		if(!digitalWatermarkFolder.exists())
			digitalWatermarkFolder.mkdirs();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
		String filename = String.format("digital_watermarking_result_%d.txt", Calendar.getInstance().getTime().getTime());
		File imageTxtFile = new File(digitalWatermarkFolder, filename);

		try
		{
			FileOutputStream fos = new FileOutputStream(imageTxtFile);
			PrintWriter pw = new PrintWriter(fos);
			pw.print(imageTxt);
			pw.flush();
			pw.close();
			fos.close();
			Toast.makeText(this, String.format("Save : %s", imageTxtFile.getPath()), Toast.LENGTH_LONG).show();
		}
		catch (FileNotFoundException e)
		{
			Utility.createPopup(this, R.string.error, e.getMessage()).show();
		}
		catch (IOException e)
		{
			Utility.createPopup(this, R.string.error, e.getMessage()).show();
		}
	}

	public class ExtractTask extends AsyncTask<Bitmap, Integer, String>
    {
	    Consumer<String> response;

	    public ExtractTask(Consumer<String> response)
	    {
		    this.response = response;
	    }

	    @Override
	    protected void onPreExecute()
	    {
		    dialog.setProgress(0);
		    dialog.show();
	    }

	    @Override
	    protected String doInBackground(Bitmap... params)
	    {
		    Bitmap image = params[0];
		    float totalTask = image.getHeight() * image.getWidth();
		    float completedTask = 0;
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < image.getHeight(); i++)
		    {
			    for (int j = 0; j < image.getWidth(); j++)
			    {
				    float[] hsv = new float[3];
				    int argb = image.getPixel(j, i);
				    Color.RGBToHSV(Color.red(argb), Color.green(argb), Color.blue(argb), hsv);
				    if(j > 0)
					    sb.append(",");
				    sb.append(hsv[0]);
				    completedTask++;
				    publishProgress((int) (completedTask * 100/ totalTask));
			    }
			    sb.append("\n");
		    }
		    return sb.toString();
	    }

	    @Override
	    protected void onProgressUpdate(Integer... values)
	    {
		    dialog.setProgress(values[0]);
	    }

	    @Override
	    protected void onPostExecute(String imageTxt)
	    {
		    dialog.dismiss();
		    if(response != null)
		    	response.accept(imageTxt);
	    }
    }
}


