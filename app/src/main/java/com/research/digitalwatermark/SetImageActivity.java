package com.research.digitalwatermark;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;


public class SetImageActivity extends AppCompatActivity
{
	private int PICK_IMAGE_REQUEST = 1;
	private ImageView imageHolder;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_image);
		imageHolder = (ImageView) findViewById(R.id.image_holder);
		setImageHolderBitmap((DigitalWatermark.getInstance().getOriginalImage()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_set_image, menu);
		return true;
	}

	public void browse(View view)
	{
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
	}

	public void ok(View view)
	{
		if(DigitalWatermark.getInstance().getOriginalImage() != null)
		{
			Intent intent = new Intent(this, SetMessageActivity.class);
			DigitalWatermark.getInstance().setOriginalImage(((BitmapDrawable) imageHolder.getDrawable()).getBitmap());
			startActivity(intent);
		}
		else
		{
			Toast.makeText(getApplicationContext(), R.string.image_not_set, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
		{
			Uri uri = data.getData();
			try
			{
				Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
				if(bitmap.getWidth() % 2 != 0)
				{
					Utility.createPopup(this, R.string.error, "Image is not power of 2").show();
					return;
				}
				setImageHolderBitmap(bitmap);
				DigitalWatermark.getInstance().setOriginalImage(bitmap);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				Utility.createPopup(this, R.string.error, e.getMessage()).show();
			}
		}
	}

	private void setImageHolderBitmap(Bitmap bitmap)
	{
		if(bitmap == null)
			return;
		imageHolder.setImageBitmap(bitmap);
	}
}
