package com.research.digitalwatermark.lens;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.research.digitalwatermark.Constants;
import com.research.digitalwatermark.DigitalWatermark;
import com.research.digitalwatermark.ErrorCode;
import com.research.digitalwatermark.R;
import com.research.digitalwatermark.SetImageDecryptActivity;
import com.research.digitalwatermark.Utility;
import com.research.digitalwatermark.vuforia.CubeShaders;
import com.research.digitalwatermark.vuforia.StartVuforiaTask;
import com.research.digitalwatermark.vuforia.Texture;
import com.research.digitalwatermark.vuforia.Triangle;
import com.research.digitalwatermark.vuforia.VideoBackgroundShader;
import com.vuforia.COORDINATE_SYSTEM_TYPE;
import com.vuforia.CameraCalibration;
import com.vuforia.CameraDevice;
import com.vuforia.Device;
import com.vuforia.GLTextureUnit;
import com.vuforia.Matrix34F;
import com.vuforia.Matrix44F;
import com.vuforia.Mesh;
import com.vuforia.ObjectTarget;
import com.vuforia.ObjectTracker;
import com.vuforia.Renderer;
import com.vuforia.RenderingPrimitives;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.VIDEO_BACKGROUND_REFLECTION;
import com.vuforia.VIEW;
import com.vuforia.Vec2F;
import com.vuforia.Vec2I;
import com.vuforia.Vec4I;
import com.vuforia.VideoBackgroundConfig;
import com.vuforia.VideoMode;
import com.vuforia.ViewList;
import com.vuforia.Vuforia;

import java.util.Vector;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class CaptureFragment extends Fragment implements Vuforia.UpdateCallbackInterface
{
	private SetImageDecryptActivity parentActivity;
	private ProgressDialog dialog;
	private FrameLayout cameraPreviewLayout;
	private View container;
	private TextView focusSupportedText;
	private ImageButton takeAPictureButton;
	private GLSurfaceView mSurface;
	private RenderingPrimitives mRenderingPrimitives;
	// Display size of the device:
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	private int vbShaderProgramID;
	private int vbTexSampler2DHandle;
	private int vbProjectionMatrixHandle;
	private int vbVertexHandle;
	private int vbTexCoordHandle;
	private GLTextureUnit videoBackgroundTex;
	private Renderer mRRenderer = null;
	private float mNearPlane = -1.0f;
	private float mFarPlane = -1.0f;
	private int currentView = VIEW.VIEW_SINGULAR;
	private float kBuildingScale = 0.012f;
	private Vector<Texture> mTextures;
	private int shaderProgramID;
	private int vertexHandle;
	private int textureCoordHandle;
	private int mvpMatrixHandle;
	private int texSampler2DHandle;
	private Triangle triangle;
	private boolean enableTakePicture;
	private int mViewWidth = 0;
	private int mViewHeight = 0;
	private boolean isTakeAPicture;
	private boolean isProcessingTakedPicture;
	private Vec2F markerUpperLeftCorner;
	private Vec2F markerUpperRightCorner;
	private Vec2F markerLowerLeftCorner;
	private Vec2F markerLowerRightCorner;
	private Vec2F center;
	private ImageView dot1;
	private ImageView dot2;
	private ImageView dot3;
	private ImageView dot4;

	private static final float OBJECT_SCALE_FLOAT = 0.003f;
	static final float VIRTUAL_FOV_Y_DEGS = 85.0f;
	static final float M_PI = 3.14159f;

	GLSurfaceView.EGLContextFactory mEGLContextFactory = new GLSurfaceView.EGLContextFactory()
	{
		private int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
		@Override
		public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig)
		{
			EGLContext context;
			int[] attrib_list_gl20 = { EGL_CONTEXT_CLIENT_VERSION, 2,
					EGL10.EGL_NONE };
			context = egl.eglCreateContext(display, eglConfig,
			                               EGL10.EGL_NO_CONTEXT, attrib_list_gl20);
			return context;
		}

		@Override
		public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context)
		{
			egl.eglDestroyContext(display, context);
		}
	};

	GLSurfaceView.Renderer mRenderer = new GLSurfaceView.Renderer()
	{
		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config)
		{
			Log.i(Constants.log, "onSurfaceCreated");
			Vuforia.onSurfaceCreated();
			initRendering();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height)
		{
			Log.i(Constants.log, "onSurfaceChanged : width : " + width + ", height : " + height);
			Vuforia.onSurfaceChanged(width, height);
			onConfigurationChanged();
			initRendering2();

			mViewWidth = width;
			mViewHeight = height;
		}

		@Override
		public void onDrawFrame(GL10 gl)
		{
			Log.i(Constants.log, "onDrawFrame");

			GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			State state;
			// Get our current state
			state = TrackerManager.getInstance().getStateUpdater().updateState();
			mRRenderer.begin(state);

			// We must detect if background reflection is active and adjust the
			// culling direction.
			// If the reflection is active, this means the post matrix has been
			// reflected as well,
			// therefore standard counter clockwise face culling will result in
			// "inside out" models.
			if (Renderer.getInstance().getVideoBackgroundConfig().getReflection() == VIDEO_BACKGROUND_REFLECTION.VIDEO_BACKGROUND_REFLECTION_ON)
				GLES20.glFrontFace(GLES20.GL_CW);  // Front camera
			else
				GLES20.glFrontFace(GLES20.GL_CCW);   // Back camera

			// We get a list of views which depend on the mode we are working on, for mono we have
			// only one view, in stereo we have three: left, right and postprocess
			ViewList viewList = mRenderingPrimitives.getRenderingViews();

			// Cycle through the view list
			for (int v = 0; v < viewList.getNumViews(); v++)
			{
				// Get the view id
				int viewID = viewList.getView(v);

				Vec4I viewport;
				// Get the viewport for that specific view
				viewport = mRenderingPrimitives.getViewport(viewID);

				// Set viewport for current view
				GLES20.glViewport(viewport.getData()[0], viewport.getData()[1], viewport.getData()[2], viewport.getData()[3]);

				// Set scissor
				GLES20.glScissor(viewport.getData()[0], viewport.getData()[1], viewport.getData()[2], viewport.getData()[3]);

				// Get projection matrix for the current view. COORDINATE_SYSTEM_CAMERA used for AR and
				// COORDINATE_SYSTEM_WORLD for VR
				Matrix34F projMatrix = mRenderingPrimitives.getProjectionMatrix(viewID, COORDINATE_SYSTEM_TYPE.COORDINATE_SYSTEM_CAMERA,
				                                                                state.getCameraCalibration());

				// Create GL matrix setting up the near and far planes
				float rawProjectionMatrixGL[] = Tool.convertPerspectiveProjection2GLMatrix(
						projMatrix,
						mNearPlane,
						mFarPlane)
						.getData();

				// Apply the appropriate eye adjustment to the raw projection matrix, and assign to the global variable
				float eyeAdjustmentGL[] = Tool.convert2GLMatrix(mRenderingPrimitives
						                                                .getEyeDisplayAdjustmentMatrix(viewID)).getData();

				float projectionMatrix[] = new float[16];
				// Apply the adjustment to the projection matrix
				Matrix.multiplyMM(projectionMatrix, 0, rawProjectionMatrixGL, 0, eyeAdjustmentGL, 0);

				currentView = viewID;

				// Call renderFrame from the app renderer class which implements SampleAppRendererControl
				// This will be called for MONO, LEFT and RIGHT views, POSTPROCESS will not render the
				// frame
				if(currentView != VIEW.VIEW_POSTPROCESS)
					renderFrame(state, projectionMatrix);
			}
			GLES20.glFinish();
			mRRenderer.end();

			if(isTakeAPicture && !isProcessingTakedPicture)
			{
				isProcessingTakedPicture = true;

				Bitmap bitmap = Utility.grabPixels(0, 0, mViewWidth, mViewHeight);
				float angle = Utility.angleVec2FinDegrees(markerUpperLeftCorner, markerUpperRightCorner);


				double width = Utility.magnitudeOfVec2F(new Vec2F(
						markerUpperRightCorner.getData()[0] - markerUpperLeftCorner.getData()[0],
						markerUpperRightCorner.getData()[1] - markerUpperLeftCorner.getData()[1]
				));
				double height = Utility.magnitudeOfVec2F(new Vec2F(
						markerLowerLeftCorner.getData()[0] - markerUpperLeftCorner.getData()[0],
						markerLowerLeftCorner.getData()[1] - markerUpperLeftCorner.getData()[1]
				));

				double max = Math.max(width, height);
				float scale = (int) (max / DigitalWatermark.IMAGE_WIDTH * 100);

				Vec2F centerPoint = new Vec2F((float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
				android.graphics.Matrix matrix = new android.graphics.Matrix();
				matrix.setRotate(-1 * angle, centerPoint.getData()[0], centerPoint.getData()[1]);
				Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
				Log.i(Constants.log, "Take a picture : x1 : " + markerUpperLeftCorner.getData()[0] + ", y1 : " + markerUpperLeftCorner.getData()[1]);
				markerUpperLeftCorner.setData(new float[] {
						markerUpperLeftCorner.getData()[0] - centerPoint.getData()[0] ,
						centerPoint.getData()[1] - markerUpperLeftCorner.getData()[1]
				});
				Vec2F newMarkerUpperLeftCorner = Utility.newRotationVec2F(markerUpperLeftCorner, angle);
				centerPoint.setData(new float[] {(float) rotatedBitmap.getWidth() / 2, (float) rotatedBitmap.getHeight() / 2});
				newMarkerUpperLeftCorner.setData(new float[] {
						centerPoint.getData()[0] + newMarkerUpperLeftCorner.getData()[0],
						centerPoint.getData()[1] - newMarkerUpperLeftCorner.getData()[1]
				});
				Log.i(Constants.log, "Take a picture : x2 : " + newMarkerUpperLeftCorner.getData()[0] + ", y2 : " + newMarkerUpperLeftCorner.getData()[1]);
				Log.i(Constants.log, "Take a picture : w1 : " + width + ", h1 : " + height);

				Bitmap croppedBitmap = Bitmap.createBitmap(rotatedBitmap, (int) newMarkerUpperLeftCorner.getData()[0], (int) newMarkerUpperLeftCorner.getData()[1], (int) width, (int) height);
				Bitmap scaledBitmap = Bitmap.createScaledBitmap(croppedBitmap, DigitalWatermark.IMAGE_WIDTH, DigitalWatermark.IMAGE_HEIGHT, false);
				parentActivity.onTakenPictureCompleted(scaledBitmap, angle, scale);
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		Log.i(Constants.log, "onCreateView");
		parentActivity = (SetImageDecryptActivity) getActivity();
		this.container = inflater.inflate(R.layout.fragment_capture, container, false);
		cameraPreviewLayout = (FrameLayout) this.container.findViewById(R.id.camera_preview);
		focusSupportedText = (TextView) this.container.findViewById(R.id.focus_supported);
		focusSupportedText.setText("");

		dot1 = (ImageView) this.container.findViewById(R.id.dot_1);
		dot2 = (ImageView) this.container.findViewById(R.id.dot_2);
		dot3 = (ImageView) this.container.findViewById(R.id.dot_3);
		dot4 = (ImageView) this.container.findViewById(R.id.dot_4);

		// init dialog progress view
		dialog = parentActivity.getDialog();


		// init button action listener
		setTakeAPictureActionListener(this.container);

		init();
		initSurface();
		initVideoBackground();
		addGLSurface();
		startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);

		dot1.bringToFront();
		dot2.bringToFront();
		dot3.bringToFront();
		dot4.bringToFront();
		Log.i(Constants.log, "onCreateView completed");
		return this.container;
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();

		stopCamera();

		boolean unloadTrackersResult;
		boolean deinitTrackersResult;

		// Destroy the tracking data set:
		unloadTrackersResult = doUnloadTrackersData();

		// Deinitialize the trackers:
		deinitTrackersResult = doDeinitTrackers();

		// Deinitialize Vuforia SDK:
		Vuforia.deinit();

		if (!unloadTrackersResult)
			Log.e(Constants.log, "Failed to unload trackers\' data");

		if (!deinitTrackersResult)
			Log.e(Constants.log, "Failed to deinitialize trackers");

		parentActivity.setVisible(true);
	}

	private void init()
	{
		Vuforia.registerCallback(this);
	}

	private void initSurface()
	{
		mSurface = new GLSurfaceView(getActivity());
		boolean translucent = Vuforia.requiresAlpha();
		int depth = 16;
		int stencil = 0;

		if(translucent)
		{
			mSurface.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		}

		mSurface.setEGLContextFactory(mEGLContextFactory);

		mSurface.setEGLConfigChooser(translucent ? new ConfigChooser(8, 8, 8, 8, depth, stencil) :
				                             new ConfigChooser(5, 6, 5, 0, depth, stencil));

		mRRenderer = Renderer.getInstance();

		mTextures = new Vector<Texture>();

		mSurface.setRenderer(mRenderer);
	}

	// Configures the video mode and sets offsets for the camera's image
	public void initVideoBackground()
	{
		CameraDevice cameraDevice = CameraDevice.getInstance();
		VideoMode vm = cameraDevice.getVideoMode(CameraDevice.MODE.MODE_DEFAULT);

		VideoBackgroundConfig config = new VideoBackgroundConfig();
		config.setEnabled(true);
		config.setPosition(new Vec2I(0, 0));

		int xSize = 0, ySize = 0;
		// We keep the aspect ratio to keep the video correctly rendered. If it is portrait we
		// preserve the height and scale width and vice versa if it is landscape, we preserve
		// the width and we check if the selected values fill the screen, otherwise we invert
		// the selection
		xSize = (int) (vm.getHeight() * (mScreenHeight / (float) vm
				.getWidth()));
		ySize = mScreenHeight;

		if (xSize < mScreenWidth)
		{
			xSize = mScreenWidth;
			ySize = (int) (mScreenWidth * (vm.getWidth() / (float) vm
					.getHeight()));
		}

		config.setSize(new Vec2I(xSize, ySize));

		Log.i(Constants.log, "Configure Video Background : Video (" + vm.getWidth()
				+ " , " + vm.getHeight() + "), Screen (" + mScreenWidth + " , "
				+ mScreenHeight + "), mSize (" + xSize + " , " + ySize + ")");

		Renderer.getInstance().setVideoBackgroundConfig(config);
	}


	private void addGLSurface()
	{
		// Now add the GL surface view. It is important
		// that the OpenGL ES surface view gets added
		// BEFORE the camera is started and video
		// background is configured.
		cameraPreviewLayout.addView(mSurface);
	}

	private void startAR(int camera)
	{
		StartVuforiaTask task = new StartVuforiaTask(4,  parentActivity.TOTAL_TASK, camera, this, dialog);
		task.execute();
	}

	public void onStartARCompleted(int errorCode)
	{
		if(errorCode == ErrorCode.NO_ERROR)
		{
			parentActivity.onCaptureFragmentCreated(ErrorCode.NO_ERROR);

			onConfigurationChanged();

			if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO))
			{
				// If continuous autofocus mode fails, attempt to set to a different mode
				if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO))
				{
					CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
				}
			}

			dialog.dismiss();
		}
		else
		{
			parentActivity.onOpenCaptureFragmentFailed("" + ErrorCode.VUFORIA_START_AR_FAILED);
		}
	}

	private void onConfigurationChanged()
	{
		Point size = new Point();
		parentActivity.getWindowManager().getDefaultDisplay().getRealSize(size);
		mScreenWidth = size.x;
		mScreenHeight = size.y;

		initVideoBackground();

		mRenderingPrimitives = Device.getInstance().getRenderingPrimitives();
	}

	void initRendering()
	{
		vbShaderProgramID = Utility.createProgramFromShaderSrc(VideoBackgroundShader.VB_VERTEX_SHADER,
		                                               VideoBackgroundShader.VB_FRAGMENT_SHADER);

		// Rendering configuration for video background
		if (vbShaderProgramID > 0)
		{
			// Activate shader:
			GLES20.glUseProgram(vbShaderProgramID);

			// Retrieve handler for texture sampler shader uniform variable:
			vbTexSampler2DHandle = GLES20.glGetUniformLocation(vbShaderProgramID, "texSampler2D");

			// Retrieve handler for projection matrix shader uniform variable:
			vbProjectionMatrixHandle = GLES20.glGetUniformLocation(vbShaderProgramID, "projectionMatrix");

			vbVertexHandle = GLES20.glGetAttribLocation(vbShaderProgramID, "vertexPosition");
			vbTexCoordHandle = GLES20.glGetAttribLocation(vbShaderProgramID, "vertexTexCoord");
			vbProjectionMatrixHandle = GLES20.glGetUniformLocation(vbShaderProgramID, "projectionMatrix");
			vbTexSampler2DHandle = GLES20.glGetUniformLocation(vbShaderProgramID, "texSampler2D");

			// Stop using the program
			GLES20.glUseProgram(0);
		}

		videoBackgroundTex = new GLTextureUnit();
	}

	private void initRendering2()
	{
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
				: 1.0f);

		for (Texture t : mTextures)
		{
			GLES20.glGenTextures(1, t.mTextureID, 0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
			                       GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
			                       GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
			                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
			                    GLES20.GL_UNSIGNED_BYTE, t.mData);
		}

		shaderProgramID = Utility.createProgramFromShaderSrc(
				CubeShaders.CUBE_MESH_VERTEX_SHADER,
				CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

		vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
		                                          "vertexPosition");
		textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
		                                                "vertexTexCoord");
		mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
		                                              "modelViewProjectionMatrix");
		texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID,
		                                                 "texSampler2D");

		triangle = new Triangle();
	}

	// The render function called from SampleAppRendering by using RenderingPrimitives views.
	// The state is owned by SampleAppRenderer which is controlling it's lifecycle.
	// State should not be cached outside this method.
	public void renderFrame(State state, float[] projectionMatrix)
	{
		// Renders video background replacing Renderer.DrawVideoBackground()
		renderVideoBackground();

		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		// handle face culling, we need to detect if we are using reflection
		// to determine the direction of the culling
		GLES20.glEnable(GLES20.GL_CULL_FACE);
		GLES20.glCullFace(GLES20.GL_BACK);

		// Did we find any trackables this frame?
		for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
			TrackableResult result = state.getTrackableResult(tIdx);
			Trackable trackable = result.getTrackable();
			Matrix34F pose = result.getPose();
			Matrix44F modelViewMatrix_Vuforia = Tool
					.convertPose2GLMatrix(pose);
			float[] modelViewMatrix = modelViewMatrix_Vuforia.getData();

			ObjectTarget detectedObject = (ObjectTarget) trackable;
			CameraCalibration cameraCalibration = state.getCameraCalibration();
			int screenOrientation = parentActivity.getRequestedOrientation();
			Log.i(Constants.log, "Find : " + detectedObject.getName() + ", size : " + detectedObject.getSize().getData()[0] + ", " +
					detectedObject.getSize().getData()[1] + ", " + detectedObject.getSize().getData()[2]
			);

			// deal with the modelview and projection matrices
			float[] modelViewProjection = new float[16];

			if (!true) {
				Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f,
				                  OBJECT_SCALE_FLOAT);
				Matrix.scaleM(modelViewMatrix, 0, OBJECT_SCALE_FLOAT,
				              OBJECT_SCALE_FLOAT, OBJECT_SCALE_FLOAT);
			} else {
				Matrix.rotateM(modelViewMatrix, 0, 90.0f, 1.0f, 0, 0);
				Matrix.scaleM(modelViewMatrix, 0, kBuildingScale,
				              kBuildingScale, kBuildingScale);
			}
			Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrix, 0);
			triangle.draw(modelViewProjection);


			// activate the shader program and bind the vertex/normal/tex coords
			GLES20.glUseProgram(shaderProgramID);

			// corner
			center = Utility.getMarkerCenter(cameraCalibration, pose, mViewWidth, mViewHeight, screenOrientation);
			markerUpperLeftCorner = Utility.getMarkerUpperLeftCorner(detectedObject, cameraCalibration, pose, mViewWidth, mViewHeight, screenOrientation);
			markerUpperRightCorner = Utility.getMarkerUpperRightCorner(detectedObject, cameraCalibration, pose, mViewWidth, mViewHeight, screenOrientation);
			markerLowerLeftCorner = Utility.getMarkerLowerLeftCorner(detectedObject, cameraCalibration, pose, mViewWidth, mViewHeight, screenOrientation);
			markerLowerRightCorner = Utility.getMarkerLowerRightCorner(detectedObject, cameraCalibration, pose, mViewWidth, mViewHeight, screenOrientation);
//			Log.i(Constants.log, "center : " + center.getData()[0] + ", " + center.getData()[1]);
//			Log.i(Constants.log, "upper left : " + markerUpperLeftCorner.getData()[0] + ", " + markerUpperLeftCorner.getData()[1]);
//			Log.i(Constants.log, "upper right : " + markerUpperRightCorner.getData()[0] + ", " + markerUpperRightCorner.getData()[1]);
//			Log.i(Constants.log, "bottom left : " + markerLowerLeftCorner.getData()[0] + ", " + markerLowerLeftCorner.getData()[1]);
//			Log.i(Constants.log, "bottom right: " + markerLowerRightCorner.getData()[0] + ", " + markerLowerRightCorner.getData()[1]);
		}

		drawDot(state.getNumTrackableResults() > 0);

		GLES20.glDisable(GLES20.GL_DEPTH_TEST);


		enableTakePicture = state.getNumTrackableResults() > 0;
		parentActivity.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				if(enableTakePicture)
				{
					if(!takeAPictureButton.isEnabled())
					{
						takeAPictureButton.setEnabled(true);
					}
				}
				else
				{
					if(takeAPictureButton.isEnabled())
					{
						takeAPictureButton.setEnabled(false);
					}
				}
			}
		});
	}

	public void renderVideoBackground()
	{
		if(currentView == VIEW.VIEW_POSTPROCESS)
			return;

		int vbVideoTextureUnit = 0;
		// Bind the video bg texture and get the Texture ID from Vuforia
		videoBackgroundTex.setTextureUnit(vbVideoTextureUnit);
		if (!mRRenderer.updateVideoBackgroundTexture(videoBackgroundTex))
		{
			return;
		}

		float[] vbProjectionMatrix = Tool.convert2GLMatrix(
				mRenderingPrimitives.getVideoBackgroundProjectionMatrix(currentView, COORDINATE_SYSTEM_TYPE.COORDINATE_SYSTEM_CAMERA)).getData();

		// Apply the scene scale on video see-through eyewear, to scale the video background and augmentation
		// so that the display lines up with the real world
		// This should not be applied on optical see-through devices, as there is no video background,
		// and the calibration ensures that the augmentation matches the real world
		if (Device.getInstance().isViewerActive()) {
			float sceneScaleFactor = (float)getSceneScaleFactor();
			Matrix.scaleM(vbProjectionMatrix, 0, sceneScaleFactor, sceneScaleFactor, 1.0f);
		}

		GLES20.glDisable(GLES20.GL_DEPTH_TEST);
		GLES20.glDisable(GLES20.GL_CULL_FACE);
		GLES20.glDisable(GLES20.GL_SCISSOR_TEST);

		Mesh vbMesh = mRenderingPrimitives.getVideoBackgroundMesh(currentView);
		// Load the shader and upload the vertex/texcoord/index data
		GLES20.glUseProgram(vbShaderProgramID);
		GLES20.glVertexAttribPointer(vbVertexHandle, 3, GLES20.GL_FLOAT, false, 0, vbMesh.getPositions().asFloatBuffer());
		GLES20.glVertexAttribPointer(vbTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, vbMesh.getUVs().asFloatBuffer());

		GLES20.glUniform1i(vbTexSampler2DHandle, vbVideoTextureUnit);

		// Render the video background with the custom shader
		// First, we enable the vertex arrays
		GLES20.glEnableVertexAttribArray(vbVertexHandle);
		GLES20.glEnableVertexAttribArray(vbTexCoordHandle);

		// Pass the projection matrix to OpenGL
		GLES20.glUniformMatrix4fv(vbProjectionMatrixHandle, 1, false, vbProjectionMatrix, 0);

		// Then, we issue the render call
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, vbMesh.getNumTriangles() * 3, GLES20.GL_UNSIGNED_SHORT,
		                      vbMesh.getTriangles().asShortBuffer());

		// Finally, we disable the vertex arrays
		GLES20.glDisableVertexAttribArray(vbVertexHandle);
		GLES20.glDisableVertexAttribArray(vbTexCoordHandle);
	}

	private void stopCamera()
	{
		doStopTrackers();
		CameraDevice.getInstance().stop();
		CameraDevice.getInstance().deinit();
	}

	private void doStopTrackers()
	{
		Tracker objectTracker = TrackerManager.getInstance().getTracker(
				ObjectTracker.getClassType());
		if (objectTracker != null)
			objectTracker.stop();
	}

	private boolean doUnloadTrackersData()
	{
		// Indicate if the trackers were unloaded correctly
		boolean result = true;

		TrackerManager tManager = TrackerManager.getInstance();
		ObjectTracker objectTracker = (ObjectTracker) tManager
				.getTracker(ObjectTracker.getClassType());
		if (objectTracker == null)
			return false;

		if (DigitalWatermark.getInstance().getmCurrentDataset() != null && DigitalWatermark.getInstance().getmCurrentDataset().isActive())
		{
			if (objectTracker.getActiveDataSet(0).equals(DigitalWatermark.getInstance().getmCurrentDataset())
					&& !objectTracker.deactivateDataSet(DigitalWatermark.getInstance().getmCurrentDataset()))
			{
				result = false;
			} else if (!objectTracker.destroyDataSet(DigitalWatermark.getInstance().getmCurrentDataset()))
			{
				result = false;
			}

			DigitalWatermark.getInstance().setmCurrentDataset(null);
		}

		return result;
	}

	public boolean doDeinitTrackers()
	{
		// Indicate if the trackers were deinitialized correctly
		boolean result = true;

		TrackerManager tManager = TrackerManager.getInstance();
		tManager.deinitTracker(ObjectTracker.getClassType());

		return result;
	}


	private void setTakeAPictureActionListener(View view) {
		takeAPictureButton = (ImageButton) view.findViewById(R.id.take_a_picture_btn);
		takeAPictureButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.i(Constants.log, "Take a picture");
				isTakeAPicture = true;
			}
		});
	}

	@Override
	public void Vuforia_onUpdate(State state)
	{
		// Log.i(Constants.log, "Vuforia_onUpdate");
	}

	private void drawDot(final boolean isObjectTracked)
	{
		parentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(isObjectTracked)
				{
					float angle = Utility.angleVec2FinDegrees(markerUpperLeftCorner, markerUpperRightCorner);
					float width = Utility.distanceVec2F(markerUpperLeftCorner, markerUpperRightCorner);
					float height = Utility.distanceVec2F(markerLowerLeftCorner, markerUpperLeftCorner);
					float max = Math.max(width, height);
					float scale = (int) (max / DigitalWatermark.IMAGE_WIDTH * 100);
					focusSupportedText.setText("angle : " + angle + "\n" + "scale : " + scale);

					RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(20, 20);
					params1.leftMargin = Math.round(markerUpperLeftCorner.getData()[0]) - 10;
					params1.topMargin = Math.round(markerUpperLeftCorner.getData()[1]) - 10;
					dot1.setLayoutParams(params1);

					RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(20, 20);
					params2.leftMargin = Math.round(markerUpperRightCorner.getData()[0]) - 10;
					params2.topMargin = Math.round(markerUpperRightCorner.getData()[1]) - 10;
					dot2.setLayoutParams(params2);

					RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(20, 20);
					params3.leftMargin = Math.round(markerLowerLeftCorner.getData()[0]) - 10;
					params3.topMargin = Math.round(markerLowerLeftCorner.getData()[1]) - 10;
					dot3.setLayoutParams(params3);

					RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(20, 20);
					params4.leftMargin = Math.round(markerLowerRightCorner.getData()[0]) - 10;
					params4.topMargin = Math.round(markerLowerRightCorner.getData()[1]) - 10;
					dot4.setLayoutParams(params4);

					dot1.setVisibility(View.VISIBLE);
					dot2.setVisibility(View.VISIBLE);
					dot3.setVisibility(View.VISIBLE);
					dot4.setVisibility(View.VISIBLE);
				}
				else
				{
					focusSupportedText.setText("angle : " + 0 + "\n" + "width : " + 0 + "\n" + "height : " + 0);
					dot1.setVisibility(View.INVISIBLE);
					dot2.setVisibility(View.INVISIBLE);
					dot3.setVisibility(View.INVISIBLE);
					dot4.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	double getSceneScaleFactor()
	{
		// Get the y-dimension of the physical camera field of view
		Vec2F fovVector = CameraDevice.getInstance().getCameraCalibration().getFieldOfViewRads();
		float cameraFovYRads = fovVector.getData()[1];

		// Get the y-dimension of the virtual camera field of view
		float virtualFovYRads = VIRTUAL_FOV_Y_DEGS * M_PI / 180;

		// The scene-scale factor represents the proportion of the viewport that is filled by
		// the video background when projected onto the same plane.
		// In order to calculate this, let 'd' be the distance between the cameras and the plane.
		// The height of the projected image 'h' on this plane can then be calculated:
		//   tan(fov/2) = h/2d
		// which rearranges to:
		//   2d = h/tan(fov/2)
		// Since 'd' is the same for both cameras, we can combine the equations for the two cameras:
		//   hPhysical/tan(fovPhysical/2) = hVirtual/tan(fovVirtual/2)
		// Which rearranges to:
		//   hPhysical/hVirtual = tan(fovPhysical/2)/tan(fovVirtual/2)
		// ... which is the scene-scale factor
		return Math.tan(cameraFovYRads / 2) / Math.tan(virtualFovYRads / 2);
	}

	// The config chooser.
	private static class ConfigChooser implements
	                                   GLSurfaceView.EGLConfigChooser
	{
		public ConfigChooser(int r, int g, int b, int a, int depth, int stencil)
		{
			mRedSize = r;
			mGreenSize = g;
			mBlueSize = b;
			mAlphaSize = a;
			mDepthSize = depth;
			mStencilSize = stencil;
		}


		private EGLConfig getMatchingConfig(EGL10 egl, EGLDisplay display,
				int[] configAttribs)
		{
			// Get the number of minimally matching EGL configurations
			int[] num_config = new int[1];
			egl.eglChooseConfig(display, configAttribs, null, 0, num_config);

			int numConfigs = num_config[0];
			if (numConfigs <= 0)
				throw new IllegalArgumentException("No matching EGL configs");

			// Allocate then read the array of minimally matching EGL configs
			EGLConfig[] configs = new EGLConfig[numConfigs];
			egl.eglChooseConfig(display, configAttribs, configs, numConfigs,
			                    num_config);

			// Now return the "best" one
			return chooseConfig(egl, display, configs);
		}


		public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
		{
			// This EGL config specification is used to specify 2.0
			// rendering. We use a minimum size of 4 bits for
			// red/green/blue, but will perform actual matching in
			// chooseConfig() below.
			final int EGL_OPENGL_ES2_BIT = 0x0004;
			final int[] s_configAttribs_gl20 = { EGL10.EGL_RED_SIZE, 4,
					EGL10.EGL_GREEN_SIZE, 4, EGL10.EGL_BLUE_SIZE, 4,
					EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
					EGL10.EGL_NONE };

			return getMatchingConfig(egl, display, s_configAttribs_gl20);
		}


		public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display,
				EGLConfig[] configs)
		{
			for (EGLConfig config : configs)
			{
				int d = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_DEPTH_SIZE, 0);
				int s = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_STENCIL_SIZE, 0);

				// We need at least mDepthSize and mStencilSize bits
				if (d < mDepthSize || s < mStencilSize)
					continue;

				// We want an *exact* match for red/green/blue/alpha
				int r = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_RED_SIZE, 0);
				int g = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_GREEN_SIZE, 0);
				int b = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_BLUE_SIZE, 0);
				int a = findConfigAttrib(egl, display, config,
				                         EGL10.EGL_ALPHA_SIZE, 0);

				if (r == mRedSize && g == mGreenSize && b == mBlueSize
						&& a == mAlphaSize)
					return config;
			}

			return null;
		}


		private int findConfigAttrib(EGL10 egl, EGLDisplay display,
				EGLConfig config, int attribute, int defaultValue)
		{

			if (egl.eglGetConfigAttrib(display, config, attribute, mValue))
				return mValue[0];

			return defaultValue;
		}

		// Subclasses can adjust these values:
		protected int mRedSize;
		protected int mGreenSize;
		protected int mBlueSize;
		protected int mAlphaSize;
		protected int mDepthSize;
		protected int mStencilSize;
		private int[] mValue = new int[1];
	}
}
