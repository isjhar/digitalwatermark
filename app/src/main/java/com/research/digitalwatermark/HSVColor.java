package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 8/5/2017.
 */

public class HSVColor
{
	private float hue;
	private float saturation;
	private float value;
	private float alpha;

	public HSVColor(float hue, float saturation, float value, float alpha)
	{
		this.hue = hue;
		this.saturation = saturation;
		this.value = value;
		this.alpha = alpha;
	}

	public float getAlpha()
	{
		return alpha;
	}

	public float getHue()
	{
		return hue;
	}

	public float getSaturation()
	{
		return saturation;
	}

	public float getValue()
	{
		return value;
	}
}
