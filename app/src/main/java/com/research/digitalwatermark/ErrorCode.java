package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 11/25/2017.
 */

public class ErrorCode
{
	public static final int NO_ERROR = 0;
	public static final int CAMERA_NOT_FOUND = 1001;
	public static final int CAMERA_PARAMETER_NULL = 1002;
	public static final int CAMERA_NO_SIZE_SUPPORTED = 1003;
	public static final int CAMERA_NO_PREVIEW_SIZE_SUPPORTED = 1004;
	public static final int CAMERA_FOCUS_NOT_SUPPORTED = 1005;
	public static final int VUFORIA_INIT_FAILED = 1006;
	public static final int VUFORIA_INIT_TRACKER_FAILED = 1007;
	public static final int VUFORIA_LOAD_TRACKER_FAILED = 1008;
	public static final int VUFORIA_START_AR_FAILED = 1009;
}
