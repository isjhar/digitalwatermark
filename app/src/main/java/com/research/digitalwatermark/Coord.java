package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 9/2/2017.
 */

public class Coord
{
	private int x;
	private int y;

	public Coord(int x, int y)
	{
		this.x = x;
		this.y = y;
	}


	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	@Override
	public boolean equals(Object o)
	{
		try
		{
			Coord coord = (Coord) o;
			return this.x == coord.getX() && this.y == coord.getY();
		}
		catch(Exception e)
		{
			return false;
		}
	}

	@Override
	public String toString()
	{
		return String.format("[x:%d, y:%d]", x, y);
	}
}
