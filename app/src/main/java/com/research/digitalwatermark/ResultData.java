package com.research.digitalwatermark;

import android.graphics.Bitmap;

/**
 * Created by Isjhar-pc on 3/8/2018.
 */

public class ResultData
{
	private Bitmap mImage;
	private float mAngle;
	private float mScale;
	private MacroBlock[] mMacroBlocks;
	private boolean mIsValid;

	public ResultData(Bitmap image, float angle, float scale, MacroBlock[] macroBlock, boolean isValid)
	{
		mImage = image;
		mAngle = angle;
		mScale = scale;
		mMacroBlocks = macroBlock;
		mIsValid = isValid;
	}

	public Bitmap getmImage()
	{
		return mImage;
	}

	public void setmImage(Bitmap mImage)
	{
		this.mImage = mImage;
	}

	public float getmAngle()
	{
		return mAngle;
	}

	public void setmAngle(float mAngle)
	{
		this.mAngle = mAngle;
	}

	public float getmScale()
	{
		return mScale;
	}

	public void setmScale(float mScale)
	{
		this.mScale = mScale;
	}

	public MacroBlock[] getmMacroBlocks()
	{
		return mMacroBlocks;
	}

	public void setmMacroBlocks(MacroBlock[] mMacroBlocks)
	{
		this.mMacroBlocks = mMacroBlocks;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("is valid : " + mIsValid + "\n");
		sb.append("angle : " + mAngle + "\n");
		sb.append("scale : " + mScale + "\n");
		sb.append("macroblock : \n");
		for (int i = 0; i < mMacroBlocks.length; i++)
		{
			sb.append("--" + (i+1) + ". " + mMacroBlocks[i].getmBitError() + " error, threshold : " + mMacroBlocks[i].getmThreshold() + "\n");
		}
		return sb.toString();
	}
}
