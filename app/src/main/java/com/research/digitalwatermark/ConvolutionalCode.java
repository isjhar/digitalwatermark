package com.research.digitalwatermark;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Isjhar-pc on 3/29/2017.
 */
public class ConvolutionalCode
{
	public static final int k = 3;
	public static final int rate = 2;
	public static byte[] generatorOutput = new byte[rate];

	public static byte[] encode(byte[] message)
	{
		byte[] result = new byte[rate * message.length];
		byte[] generator = createGenerator();

		for(int i = 0; i < message.length; i++)
		{
			generator = insertBitToGenerator(generator, message[i]);
			generatorOutput = generate(generator);
			for(int j = 0; j < generatorOutput.length; j++)
				result[(i * generatorOutput.length) + j] = generatorOutput[j];
		}
		return result;
	}

	public static byte[] decode(byte[] message) throws NumberFormatException, NullPointerException
	{
		if(message.length % 2 != 0 || message.length <= 4)
			throw new NumberFormatException("length of message invalid");

		Log.i(Constants.log, "Start Decode");
		List<Solution> possibleSolutions = initializeSolution(message);
		HashMap<Integer, Solution> stateAndItsSolutions = initializeStateAndItsSolution();
		Log.i(Constants.log, "initial solutions : " + possibleSolutions.size());
		for (int i = 0; i < message.length - 4; i+= 2)
		{
//			Log.i(Constants.log, "Iterate : " + (i + 1));
			resetStateAndItsSolutions(stateAndItsSolutions);
			for (int j = 0; j < possibleSolutions.size(); j++)
			{
				Solution[] nextSolution = generateNextStateOf(possibleSolutions.get(j));
				for (int k = 0; k < nextSolution.length; k++)
				{
					addSolutionToItsState(stateAndItsSolutions, nextSolution[k]);
				}
			}
			reconfigurePossibleSolutions(stateAndItsSolutions, possibleSolutions);
//			for (int j = 0; j < possibleSolutions.size(); j++)
//			{
//				Log.i(Constants.log, "Message : " + Utility.binariesToString(possibleSolutions.get(j).getMessageBuilder()) +
//						", Decoded message : " + Utility.binariesToString(possibleSolutions.get(j).getDecodedMessageBuilder()));
//			}
		}

		return getMinimumSolution(possibleSolutions);
	}

	private static byte[] getMinimumSolution(List<Solution> solutions)
	{
		Solution min = solutions.get(0);
		for (int i = 1; i < solutions.size(); i++)
		{
			if (solutions.get(i).getDistance() < min.getDistance())
				min = solutions.get(i);
		}
		return min.getResult();
	}
	private static void reconfigurePossibleSolutions(HashMap<Integer, Solution> state, List<Solution> solutions)
			throws NullPointerException
	{
		solutions.clear();
		for(Map.Entry<Integer, Solution> entry : state.entrySet())
		{
			if(entry.getValue() != null)
			{
				solutions.add(entry.getValue());
//				Log.i(Constants.log, entry.getValue().toString());
			}
			else
			{
				throw new NullPointerException("Failed aad not possible solution, value is null");
			}
		}
	}

	private static void addSolutionToItsState(HashMap<Integer, Solution> state, Solution solution)
	{
		if(state.get(solution.getCurrState()) == null)
		{
			state.put(solution.getCurrState(), solution);
			return;
		}

		if(solution.getDistance() <= state.get(solution.getCurrState()).getDistance()){
			state.put(solution.getCurrState(), solution);
		}
	}

	public static void resetStateAndItsSolutions(HashMap<Integer, Solution> stateAndItsSolutions)
	{
		for(Map.Entry<Integer, Solution> entry : stateAndItsSolutions.entrySet())
		{
			stateAndItsSolutions.put(entry.getKey(), null);
		}
	}

	public static HashMap<Integer, Solution> initializeStateAndItsSolution()
	{
		HashMap<Integer, Solution> stateAndItsSolutions = new HashMap<>();
		stateAndItsSolutions.put(0, null);
		stateAndItsSolutions.put(1, null);
		stateAndItsSolutions.put(2, null);
		stateAndItsSolutions.put(3, null);
		return stateAndItsSolutions;
	}

	public static List<Solution> initializeSolution(byte[] message)
	{
		Solution firstSolution = new Solution(message);
		firstSolution.setCurrState(0);
		List<Solution> solutions = new ArrayList<>();
		solutions.add(firstSolution);
		for (int i = 0; i < 3; i++)
		{
			Solution currSolution = solutions.remove(0);
			Solution[] nextSolution = generateNextStateOf(currSolution);
			for (int j = 0; j < nextSolution.length; j++)
			{
				solutions.add(nextSolution[j]);
			}
		}
		return solutions;
	}

	public static Solution[] generateNextStateOf(Solution solution)
	{
		Solution[] result = new Solution[2];
		switch (solution.getCurrState())
		{
			case 0:
				Solution zero = new Solution(solution);
				zero.add((byte) 0, new byte[] {0, 0});
				zero.setCurrState(0);
				Solution one = new Solution(solution);
				one.add((byte) 1, new byte[]{1, 1});
				one.setCurrState(1);
				result[0] = zero;
				result[1] = one;
				break;
			case 1:
				zero = new Solution(solution);
				zero.add((byte) 0, new byte[] {0, 1});
				zero.setCurrState(2);
				one = new Solution(solution);
				one.add((byte) 1, new byte[]{1, 0});
				one.setCurrState(3);
				result[0] = zero;
				result[1] = one;
				break;
			case 2:
				zero = new Solution(solution);
				zero.add((byte) 0, new byte[] {1, 1});
				zero.setCurrState(0);
				one = new Solution(solution);
				one.add((byte) 1, new byte[]{0, 0});
				one.setCurrState(1);
				result[0] = zero;
				result[1] = one;
				break;
			case 3:
				zero = new Solution(solution);
				zero.add((byte) 0, new byte[] {1, 0});
				zero.setCurrState(2);
				one = new Solution(solution);
				one.add((byte) 1, new byte[]{0, 1});
				one.setCurrState(3);
				result[0] = zero;
				result[1] = one;
				break;
		}
		return result;
	}

	private static void insertSolution(List<Solution> solutions, Solution solution)
	{
		if(!solutions.isEmpty())
		{
			solutions.add(solution);
			return;
		}

		for (int i = 0; i < solutions.size(); i++)
		{
			if(solution.getDistance() < solutions.get(i).getDistance())
			{
				solutions.add(solution);
				return;
			}
		}
		solutions.add(solution);
	}

	private static byte[] createGenerator()
	{
		byte[] generator = new byte[k];
		for(int i = 0; i < generator.length; i++)
			generator[i] = 0;
		return generator;
	}

	private static byte[] generate(byte[] generator)
	{
		generatorOutput[0] = generate1(generator);
		generatorOutput[1] = generate2(generator);
		return generatorOutput;
	}

	private static byte generate1(byte[] generator)
	{
		return (byte) ((generator[0] + generator[2]) % 2);
	}

	private static byte generate2(byte[] generator)
	{
		return (byte) ((generator[0] + generator[1] +  generator[2]) % 2);
	}

	private static byte[] insertBitToGenerator(byte[] generator, byte bit)
	{
		/// shift
		for(int i = 1; i < generator.length; i++)
			generator[i - 1] = generator[i];
		generator[generator.length - 1] = bit;
		return generator;
	}

	public static class Solution
	{
		private int currState;
		private List<Byte> decodedMessageBuilder;
		private List<Byte> messageBuilder;
		private int distance;
		private byte[] actualMessage;
		private byte[] actualMessageTemp;

		public Solution(byte[] a)
		{
			decodedMessageBuilder = new ArrayList<>();
			messageBuilder = new ArrayList<>();
			actualMessage = a;
			actualMessageTemp = new byte[rate];
		}

		public Solution(Solution other)
		{
			currState = other.getCurrState();
			decodedMessageBuilder = new ArrayList<>();
			decodedMessageBuilder.addAll(other.getDecodedMessageBuilder());
			messageBuilder = new ArrayList<>();
			messageBuilder.addAll(other.getMessageBuilder());
			distance = other.getDistance();
			actualMessage = other.getActualMessage();
			actualMessageTemp = new byte[other.getActualMessageTemp().length];
		}

		public void add(byte m, byte[] dm) throws NumberFormatException
		{
			if(dm.length != rate)
				throw new NumberFormatException("Invalid message length");
			int startIndex = decodedMessageBuilder.size();
			for (int i = 0; i < actualMessageTemp.length; i++)
			{
				actualMessageTemp[i] = actualMessage[startIndex + i];
			}
			distance += Utility.calculateDistance(dm,  actualMessageTemp);
			for (int i = 0; i < dm.length; i++)
			{
				decodedMessageBuilder.add(dm[i]);
			}
			messageBuilder.add(m);
		}

		public int getDistance()
		{
			return distance;
		}

		public int getCurrState()
		{
			return currState;
		}

		public void setCurrState(int currState)
		{
			this.currState = currState;
		}

		public int getMessageLength()
		{
			return decodedMessageBuilder.size();
		}

		public byte[] getResult()
		{
			byte[] result = new byte[messageBuilder.size()];
			for (int i = 0; i < result.length; i++)
			{
				result[i] = messageBuilder.get(i).byteValue();
			}
			return result;
		}

		public List<Byte> getDecodedMessageBuilder()
		{
			return decodedMessageBuilder;
		}

		public void setDecodedMessageBuilder(List<Byte> decodedMessageBuilder)
		{
			this.decodedMessageBuilder = decodedMessageBuilder;
		}

		public List<Byte> getMessageBuilder()
		{
			return messageBuilder;
		}

		public void setMessageBuilder(List<Byte> messageBuilder)
		{
			this.messageBuilder = messageBuilder;
		}

		public byte[] getActualMessage()
		{
			return actualMessage;
		}

		public void setActualMessage(byte[] actualMessage)
		{
			this.actualMessage = actualMessage;
		}

		public byte[] getActualMessageTemp()
		{
			return actualMessageTemp;
		}

		public void setActualMessageTemp(byte[] actualMessageTemp)
		{
			this.actualMessageTemp = actualMessageTemp;
		}

		@Override
		public String toString()
		{
			return "Message : " + Utility.binariesToString(messageBuilder) + ", decoded : " + Utility.binariesToString(decodedMessageBuilder);
		}
	}
}
