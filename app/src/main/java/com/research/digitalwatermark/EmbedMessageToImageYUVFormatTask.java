package com.research.digitalwatermark;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.research.digitalwatermark.jtransform.DoubleFFT_2D;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Isjhar-pc on 2/6/2018.
 */

public class EmbedMessageToImageYUVFormatTask extends AsyncTask<Object, Integer, Bitmap>
		implements ICompleteable
{
	private int completedTask;
	private int totalTask;
	private Bitmap image;
	private byte[] message;
	private final Object lock = new Object();
	private boolean completed;
	private String errorMesage;
	private Context context;
	private YUVColor[][] YUVArr;
	private double[][] YArr;
	private ProgressDialog dialog;
	private Activity activity;

	public EmbedMessageToImageYUVFormatTask(Activity activity, Bitmap image, byte[] message , ProgressDialog dialog)
	{
		this.image = image;
		this.message = message;
		this.dialog = dialog;
		this.activity = activity;
	}

	@Override
	protected void onPreExecute()
	{
		dialog.setProgress(0);
	}

	@Override
	protected Bitmap doInBackground(Object[] params)
	{
		Log.i(Constants.log, "start processing");

		int macroWidth = image.getWidth()/DigitalWatermark.MACROBLOCK_NUM;
		int macroHeight = image.getHeight()/DigitalWatermark.MACROBLOCK_NUM;
		YArr = new double[macroHeight][macroWidth * 2];
		YUVArr = new YUVColor[macroHeight][macroWidth];
		Bitmap result = Bitmap.createBitmap(image.getWidth(), image.getHeight(), image.getConfig());
		completedTask = 0;
		totalTask = 4;
		completed = false;
		for (int i = 0; i < image.getHeight(); i+= macroHeight)
		{
			for (int j = 0; j < image.getWidth(); j+=macroWidth)
			{
				Log.i(Constants.log, "Process macroblock at start point : " + j + ", " + i);
				Bitmap macroImage = Bitmap.createBitmap(image, j, i, macroWidth, macroHeight);
				Bitmap resultImage = macroImage.copy(macroImage.getConfig(), true);
				FFT(macroImage, resultImage, YUVArr, YArr);
				if(errorMesage != null && !errorMesage.isEmpty())
					return null;
				Utility.copyPixelsRect(resultImage, result, j, i);
				complete();
			}
		}
		return result;
	}

	public void FFT(Bitmap image, Bitmap resultImage, YUVColor[][] YUVArr, double[][] YArr)
	{
		DoubleFFT_2D fft = new DoubleFFT_2D(image.getHeight(), image.getWidth());
		Utility.bitmapToYUV(image, YUVArr, YArr, DigitalWatermark.INDEX_CHANNEL);
		Utility.printArr(YArr);
		fft.complexForward(YArr);
		Utility.fftshift(YArr);

		Log.i(Constants.log, "-----------------Insert--------------------------------");
		Log.i(Constants.log, "-----------------Insert--------------------------------");
		Log.i(Constants.log, "-----------------Insert--------------------------------");;

		List<Coord> insertedCoord = new ArrayList<>();
		int insertedMessage = 0;
		int centerX = image.getHeight() / 2 + 1;
		int centerY = image.getWidth() / 2 + 1;
		for(int i = 0; i < message.length; i++)
		{
			errorMesage = embedMessage(i, centerX, centerY, insertedCoord);
			if(!errorMesage.isEmpty())
				return;
		}
		Utility.fftshift(YArr);
		fft.complexInverse(YArr, true);
		Log.i(Constants.log, "inverse completed");
		Utility.printArr(YArr);
		Utility.YUVToBitmap(resultImage, YUVArr, YArr, DigitalWatermark.INDEX_CHANNEL);
	}

	@Override
	protected void onProgressUpdate(Integer... values)
	{
		dialog.setProgress(values[0]);
	}

	@Override
	protected void onPostExecute(Bitmap image)
	{
		dialog.dismiss();
		if(errorMesage != null && !errorMesage.isEmpty())
		{
			Utility.createPopup(activity, R.string.error, errorMesage).show();
			Log.i(Constants.log, "processing completed");
			return;
		}

		saveImageToGallery(image);
		if(errorMesage != null && !errorMesage.isEmpty())
		{
			Utility.createPopup(activity,R.string.error, errorMesage).show();
			Log.i(Constants.log, "processing completed");
			return;
		}

		goToMainClass();
		Log.i(Constants.log, "processing completed");
	}

	private String embedMessage(int i, int centerX, int centerY, List<Coord> insertedCoord)
	{
		double cos1 = Math.cos((i) * Math.PI / message.length);
		double sin1 = Math.sin((i) * Math.PI / message.length);
		int x1 = centerX + (int) Math.round(DigitalWatermark.radius * cos1);
		int y1 = (centerY + (int) Math.round(
				DigitalWatermark.radius * sin1)) * 2;

		//				Log.i(Constants.log, String.format("cos1:%f, sin1:%f, x1:%d, y1:%d, index1:%d", cos1, sin1, x1, y1, index1));
		//				Log.i(Constants.log, String.format("cos2:%f, sin2:%f, x2:%d, y2:%d, index2:%d", cos2, sin2, x2, y2, index2));

		Log.i(Constants.log, String.format("Embed -> (%d, %d), centerX : %d, centerY : %d, cos1 : %f, sin1 : %f", x1, y1,
		                                   centerX, centerY, cos1, sin1));
		Coord coord1 = new Coord(x1, y1);
		if (insertedCoord.contains(coord1))
		{
			Log.e(Constants.log, String.format("Duplicate1:%s", coord1.toString()));
			return "Duplicate coord";
		}
		else
		{
			DigitalWatermark.getInstance().embbedMessage(message[i], YArr, x1, y1, insertedCoord);
		}
		return "";
	}

	public void complete(int x, int y, ComplexNumber value)
	{
		synchronized (lock)
		{
			completedTask++;
			publishProgress(completedTask * 100 / totalTask);
			//		Log.i(Constants.log, "dft at pixel (" + x + ", " + y + ") : " + result[x][y].toString());
			Log.i(Constants.log, "task : " + completedTask + "/" + totalTask);
			if (completedTask >= totalTask)
			{
				completed = true;
			}
		}
	}

	public void complete()
	{
		completedTask++;
		publishProgress(completedTask * 100 / totalTask);
		if(completedTask >= totalTask)
			completed = true;
	}

	private void saveImageToGallery(Bitmap image)
	{
		FileOutputStream out = null;
		try
		{
			/// check storage is available
			if(!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
				return;

			File pictureFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
			File digitalWatermarkFolder = new File(pictureFolder, "DigitalWatermarking");
			digitalWatermarkFolder.mkdirs();

			String filename = Utility.getCurrentDateInString() + "_" + "digital_watermarking_result.jpg";
			File imageFile = new File(digitalWatermarkFolder, filename);

			out = new FileOutputStream(imageFile);
			image.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
			out.close();

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				final Uri contentUri = Uri.fromFile(imageFile);
				scanIntent.setData(contentUri);
				activity.sendBroadcast(scanIntent);
			} else {
				activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))));
			}

		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			errorMesage = e.getMessage();
			return;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			errorMesage = e.getMessage();
			return;
		}
	}

	private void goToMainClass()
	{
		Intent intent = new Intent(activity, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		activity.startActivity(intent);
	}
}

