package com.research.digitalwatermark;

/**
 * Created by Isjhar-pc on 9/21/2017.
 */

public interface Consumer<T>
{
	void accept(T t);
}
