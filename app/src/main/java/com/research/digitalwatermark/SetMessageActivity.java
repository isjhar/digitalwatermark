package com.research.digitalwatermark;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class SetMessageActivity extends AppCompatActivity
{
	protected EditText messageText;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_set_message);
		messageText = (EditText) findViewById(R.id.message_text);

		dialog = new ProgressDialog(this);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setMessage("Loading. Please wait...");
		dialog.setCancelable(false);
		dialog.setIndeterminate(false);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_set_message, menu);
		return true;
	}

	public void process(View view)
	{
		if(messageText.getText().toString().length() == 0)
		{
			Toast.makeText(getApplicationContext(), R.string.message_not_defined, Toast.LENGTH_SHORT).show();
			return;
		}

		if(messageText.getText().toString().length() > DigitalWatermark.MAX_MESSAGE_LENGTH)
		{
			Toast.makeText(getApplicationContext(), R.string.long_not_enough, Toast.LENGTH_SHORT).show();
			return;
		}

		String message = messageText.getText().toString();
		byte[] processedMessage = message.getBytes();
		try
		{
			processedMessage = AESEncryption.encrypt(processedMessage);
			processedMessage = Utility.bytesToBinaries(processedMessage);
			Log.i(Constants.log, "Encrypted message : " + Utility.binariesToString(processedMessage));
			processedMessage = ConvolutionalCode.encode(processedMessage);
			Log.i(Constants.log, "Process message, length : " + processedMessage.length + " bit");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Utility.createPopup(this, R.string.error, e.getMessage()).show();
		}

		if(processedMessage.length > DigitalWatermark.getInstance().getOriginalImage().getHeight() * DigitalWatermark.getInstance().getOriginalImage().getWidth())
		{
			Toast.makeText(getApplicationContext(), R.string.message_too_long, Toast.LENGTH_SHORT).show();
			return;
		}

		Log.i(Constants.log, message + " -> : " + new String(processedMessage));
		Log.i(Constants.log, Utility.binariesToString(message.getBytes()) + " -> " +
				Utility.binariesToString(processedMessage));

		dialog.show();
		DigitalWatermark.getInstance().setMessage(message);
		EmbedMessageToImageYUVFormatTask embedMessageToImageTask = new EmbedMessageToImageYUVFormatTask(this, DigitalWatermark.getInstance().getOriginalImage(), processedMessage, dialog);
		embedMessageToImageTask.execute();
	}
}
